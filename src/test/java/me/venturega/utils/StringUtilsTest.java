package me.venturega.utils;

import org.junit.Assert;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import me.venturega.asanalyze.utils.VGStringUtils;

public class StringUtilsTest extends TestCase {

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( StringUtilsTest.class );
    }

    public void test_markdownify() {
    	String input = "<b>Blah</b>";
    	String expected = "**Blah**";
    	String actual = VGStringUtils.markdownify(input);
    	Assert.assertEquals(expected, actual);
    	
    	input = "<i>Blah</i>";
    	expected = "*Blah*";
    	actual = VGStringUtils.markdownify(input);
    	Assert.assertEquals(expected, actual);
    	
    	input = "<u>Blah</u>";
    	expected = "_Blah_";
    	actual = VGStringUtils.markdownify(input);
    	Assert.assertEquals(expected, actual);
    	
    	input = "<p>Blah</p>";
    	expected = "\nBlah\n";
    	actual = VGStringUtils.markdownify(input);
    	Assert.assertEquals(expected, actual);
    }
}
