package me.venturega.asanalyze.utils;

import static org.junit.Assert.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.junit.Ignore;
import org.junit.Test;

import macromedia.asc.parser.*;
import macromedia.asc.semantics.Value;
import macromedia.asc.util.Context;
import macromedia.asc.util.ContextStatics;

public class AS3PrettyPrinterTest {

	private String evalAsString(Node node) {
		Context cx = new Context(new ContextStatics());
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		AS3PrettyPrinter as3pp = new AS3PrettyPrinter(pw);
		node.evaluate(cx, as3pp);
		as3pp.flush();
		return standardizeEOL(sw.getBuffer().toString());
	}

	private String standardizeEOL(String str) {
		return str.replace("\r\n", "\n").replace("\r", "\n");
	}
	
	@Test
	public void testEvaluateContextAttributeListNode() {
		/*
		OK, this one is a bit odd; there's two ways of presenting the data.
		 
		When the Flex Compiler builds the ALN, it uses attrs.hasPublic.
		
		HOWEVER, you usually just see the <ListNode><MXN><GetXN><Ident> combo instead...?
		
		In any case, the Flex PrettyPrinter class uses the nodes, so that's what we'll use, at least for now.
		
        <AttributeListNode>
          <ListNode>
            <MemberExpressionNode>
              <selector>
                <GetExpressionNode is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
                  <IdentifierNode name="public"/>
                </GetExpressionNode>
              </selector>
            </MemberExpressionNode>
          </ListNode>
        </AttributeListNode>
		 */
		ListNode ln = new ListNode(null,
			new MemberExpressionNode(null,
	            new GetExpressionNode(new IdentifierNode("public", 0)), 0), 0);
		AttributeListNode attrs = new AttributeListNode(ln, 0);
		attrs.hasPublic = true;

		String expected = "public ";
		String actual = evalAsString(attrs);

		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextArgumentListNode() {
		/*
        <ArgumentListNode>
          <LiteralNumberNode value="1"/>
          <LiteralNumberNode value="2"/>
          <LiteralNumberNode value="3"/>
        </ArgumentListNode>
        */
		ArgumentListNode aln = new ArgumentListNode(new LiteralNumberNode("1"), 0);
		aln.items.add(new LiteralNumberNode("2"));
		aln.items.add(new LiteralNumberNode("3"));

		String expected = "1, 2, 3";
		String actual = evalAsString(aln);

		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextBinaryExpressionNode() {
		/*
		  value + minutes ->
	      <BinaryExpressionNode>
	        <MemberExpressionNode>
	          <selector>
	            <GetExpressionNode is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
	              <IdentifierNode name="value"/>
	            </GetExpressionNode>
	          </selector>
	        </MemberExpressionNode>
	        <MemberExpressionNode>
	          <selector>
	            <GetExpressionNode is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
	              <IdentifierNode name="minutes"/>
	            </GetExpressionNode>
	          </selector>
	        </MemberExpressionNode>
	      </BinaryExpressionNode>
		 */
		BinaryExpressionNode bxn = new BinaryExpressionNode(
				Tokens.PLUS_TOKEN, 
				new LiteralNumberNode("1"),
				new LiteralNumberNode("2")
				);
		
		String expected = "1 + 2";
		String actual = evalAsString(bxn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextCallExpressionNode() {
		ArgumentListNode aln = new ArgumentListNode(new IdentifierNode("arg1", 0), 0);
		CallExpressionNode bxn = new CallExpressionNode(new IdentifierNode("func", 0), aln);
		
		String expected = "func(arg1)";
		String actual = evalAsString(bxn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextClassDefinitionNode() {
		//attr1 class ClassName extends BaseClass implements IInterface{
		//  statementlist;
		//}
		
		// Leaving Context and PackageDefinitionNode null since I think they're only used for referential purposes.
		
		// These would generally be stuff like public, static, private, protected, etc
		AttributeListNode attrs = new AttributeListNode(new IdentifierNode("attr1", 0), 0);
		ListNode ifaces = new ListNode(null, new IdentifierNode("IInterface", 0), 0);
		ClassDefinitionNode cdn = new ClassDefinitionNode(null, null, attrs, new IdentifierNode("ClassName", 0), new IdentifierNode("BaseClass", 0), ifaces, new StatementListNode(null));
		
		String expected = "attr1 class ClassName extends BaseClass\n implements IInterface\n{\n} // class ClassName";
		String actual = evalAsString(cdn);
		
		//System.out.println(actual.replace("\n", "\\n").replace("\t", "\\t"));
		
		assertEquals(expected, actual);
	}

	// This is for the old for(a;b;c){d} stuff.
	@Test
	public void testEvaluateContextForStatementNodeClassic() {
		ForStatementNode fsn = new ForStatementNode(new IdentifierNode("initializer", 0), new IdentifierNode("condition", 0), new IdentifierNode("increment", 0), null, false, false);		
		
		String expected = "for (initializer; condition; increment)\n{\n}";
		String actual = evalAsString(fsn);
		
		//System.out.println(actual.replace("\n", "\\n").replace("\t", "\\t"));
		
		assertEquals(expected, actual);
	}

	// This is for the for(var a:String in b){c} syntax.
	@Test
	public void testEvaluateContextForStatementNodeForIn() {
		// In Flex itself, only init and test are defined, increment is left null.
		// It appears like during compile, it's changed from:
		//   for (var a:String in iterable)
		// to:
		//   var a:String;for(var i:int=0, o:*=null;iterable.hasNext(o,i);){ a=iterable.NextName(i)); ...}
		// How it's parsed:
		//   var a:String;for(; a;) { }
		
		ForStatementNode fsn = new ForStatementNode(new IdentifierNode("item", 0), new IdentifierNode("iterable", 0), null, null, true, false);		
		
		String expected = "for (item in iterable)\n{\n}";
		String actual = evalAsString(fsn);
		
		//System.out.println(actual.replace("\n", "\\n").replace("\t", "\\t"));
		
		assertEquals(expected, actual);
	}

	// This is for the for each(var a:String in b){c} syntax.
	// NOTE: This requires the patched version of Flex.
	@Test
	public void testEvaluateContextForStatementNodeForEach() {
		ForStatementNode fsn = new ForStatementNode(new IdentifierNode("item", 0), new IdentifierNode("iterable", 0), null, null, true, true);		
		
		String expected = "for each (item in iterable)\n{\n}";
		String actual = evalAsString(fsn);
		
		//System.out.println(actual.replace("\n", "\\n").replace("\t", "\\t"));
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextFunctionCommonNode() {
		ParameterListNode pln = new ParameterListNode(null, new ParameterNode(0, new IdentifierNode("arg1", 0), null, null), 0);//new Parameter("arg1", 0), 0);
		FunctionSignatureNode fsn = new FunctionSignatureNode(pln, new IdentifierNode("ReturnType", 0));
		FunctionCommonNode bxn = new FunctionCommonNode(null, null, "internal_name", new IdentifierNode("myFunction", 0), fsn, null, false);
		
		String expected = "(arg1:*):ReturnType\n{\n} // (arg1:*):ReturnType\n";
		String actual = evalAsString(bxn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextFunctionCommonNodeAnonymous() {
		AttributeListNode attrs = new AttributeListNode(new IdentifierNode("attr1", 0), 0);
		ParameterListNode pln = new ParameterListNode(null, new ParameterNode(0, new IdentifierNode("arg1", 0), null, null), 0);//new Parameter("arg1", 0), 0);
		FunctionSignatureNode fsn = new FunctionSignatureNode(pln, new IdentifierNode("ReturnType", 0));
		FunctionCommonNode bxn = new FunctionCommonNode(null, null, "internal_name", new IdentifierNode("", 0), fsn, null, false);
		
		String expected = "function(arg1:*):ReturnType\n{\n} // function(arg1:*):ReturnType\n";
		String actual = evalAsString(bxn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextFunctionDefinitionNode() {
		AttributeListNode attrs = new AttributeListNode(new IdentifierNode("attr1", 0), 0);
		ParameterListNode pln = new ParameterListNode(null, new ParameterNode(0, new IdentifierNode("arg1", 0), null, null), 0);//new Parameter("arg1", 0), 0);
		FunctionSignatureNode fsn = new FunctionSignatureNode(pln, new IdentifierNode("ReturnType", 0));
		IdentifierNode fncName = new IdentifierNode("myFunction", 0);
		FunctionCommonNode bxn = new FunctionCommonNode(null, null, "internal_name", fncName, fsn, null, false);
		FunctionDefinitionNode fdn = new FunctionDefinitionNode(null, null,attrs,new FunctionNameNode(0, fncName), bxn);
		
		String expected = "attr1 function myFunction(arg1:*):ReturnType\n{\n} // attr1 function myFunction(arg1:*):ReturnType\n";
		String actual = evalAsString(fdn);
		
		assertEquals(expected, actual);
	}
	@Test
	public void testEvaluateContextFunctionDefinitionNodeGetter() {
		AttributeListNode attrs = new AttributeListNode(new IdentifierNode("attr1", 0), 0);
		ParameterListNode pln = new ParameterListNode(null, new ParameterNode(0, new IdentifierNode("arg1", 0), null, null), 0);//new Parameter("arg1", 0), 0);
		FunctionSignatureNode fsn = new FunctionSignatureNode(pln, new IdentifierNode("ReturnType", 0));
		IdentifierNode fncName = new IdentifierNode("myFunction", 0);
		FunctionCommonNode bxn = new FunctionCommonNode(null, null, "internal_name", fncName, fsn, null, false);
		FunctionDefinitionNode fdn = new FunctionDefinitionNode(null, null,attrs,new FunctionNameNode(Tokens.GET_TOKEN, fncName), bxn);
		
		String expected = "attr1 function get myFunction(arg1:*):ReturnType\n{\n} // attr1 function get myFunction(arg1:*):ReturnType\n";
		String actual = evalAsString(fdn);
		
		assertEquals(expected, actual);
	}
	@Test
	public void testEvaluateContextFunctionDefinitionNodeSetter() {
		AttributeListNode attrs = new AttributeListNode(new IdentifierNode("attr1", 0), 0);
		ParameterListNode pln = new ParameterListNode(null, new ParameterNode(0, new IdentifierNode("arg1", 0), null, null), 0);//new Parameter("arg1", 0), 0);
		FunctionSignatureNode fsn = new FunctionSignatureNode(pln, new IdentifierNode("ReturnType", 0));
		IdentifierNode fncName = new IdentifierNode("myFunction", 0);
		FunctionCommonNode bxn = new FunctionCommonNode(null, null, "internal_name", fncName, fsn, null, false);
		FunctionDefinitionNode fdn = new FunctionDefinitionNode(null, null,attrs,new FunctionNameNode(Tokens.SET_TOKEN, fncName), bxn);
		
		String expected = "attr1 function set myFunction(arg1:*):ReturnType\n{\n} // attr1 function set myFunction(arg1:*):ReturnType\n";
		String actual = evalAsString(fdn);
		
		assertEquals(expected, actual);
	}
	

	@Test
	public void testEvaluateContextGetExpressionNodeArglistExpr() {
		GetExpressionNode gxn = new GetExpressionNode(new ArgumentListNode(new IdentifierNode("argList", 0), 0));
		MemberExpressionNode mxn = new MemberExpressionNode(
				new GetExpressionNode(new IdentifierNode("a",0)),
				gxn,
				0);
		gxn.setMode(Tokens.LEFTBRACKET_TOKEN);
		
		String expected = "a[argList]";
		String actual = evalAsString(mxn);
		
		assertEquals(expected, actual);
	}
	

	@Test
	public void testEvaluateContextGetExpressionNodeNonArglistExpr() {
		GetExpressionNode gxn = new GetExpressionNode(new IdentifierNode("argList", 0));
		
		String expected = "argList";
		String actual = evalAsString(gxn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextIdentifierNode() {
		IdentifierNode idn = new IdentifierNode("IdentifierNode", 0);
		
		String expected = "IdentifierNode";
		String actual = evalAsString(idn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextIfStatementNode() {
		IfStatementNode ifn = new IfStatementNode(new IdentifierNode("expr",0), new IdentifierNode("then", 0), new IdentifierNode("else",0));
		
		String expected = "if (expr)\n{\n\tthen\n} else {\n\telse\n}";
		String actual = evalAsString(ifn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextInterfaceDefinitionNode() {
		// These would generally be stuff like public, static, private, protected, etc
		AttributeListNode attrs = new AttributeListNode(new IdentifierNode("attr1", 0), 0);
		ListNode ifaces = new ListNode(null, new IdentifierNode("ISomeInterface", 0), 0);
		InterfaceDefinitionNode cdn = new InterfaceDefinitionNode(null, null, attrs, new IdentifierNode("IInterfaceName", 0), ifaces, new StatementListNode(null));
		
		String expected = "attr1 interface IInterfaceName\n implements ISomeInterface\n{\n} // interface IInterfaceName";
		String actual = evalAsString(cdn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralArrayNode() {
		ArgumentListNode aln = new ArgumentListNode(new IdentifierNode("a",0),0);
		aln.items.add(new IdentifierNode("b",0));
		LiteralArrayNode lan = new LiteralArrayNode(aln);
		
		String expected = "[a, b]";
		String actual = evalAsString(lan);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralVectorNode() {
		ArgumentListNode aln = new ArgumentListNode(new IdentifierNode("a",0),0);
		aln.items.add(new IdentifierNode("b",0));
		LiteralVectorNode lvn = new LiteralVectorNode(aln, null);
		lvn.type = new IdentifierNode("T", 0);
		
		String expected = "new<T>[a, b]";
		String actual = evalAsString(lvn);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralBooleanNodeT() {
		String expected = "true";
		String actual = evalAsString(new LiteralBooleanNode(true));
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralBooleanNodeF() {
		String expected = "false";
		String actual = evalAsString(new LiteralBooleanNode(false));
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralFieldNode() {
		String expected = "a: b";
		String actual = evalAsString(new LiteralFieldNode(new IdentifierNode("a",0), new IdentifierNode("b", 0)));
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralNumberNode() {
		String expected = "0.0";
		String actual = evalAsString(new LiteralNumberNode("0.0"));
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralObjectNode() {
		ArgumentListNode aln = new ArgumentListNode(new IdentifierNode("a",0),0);
		aln.items.add(new IdentifierNode("b",0));
		LiteralObjectNode node = new LiteralObjectNode(aln);
		String expected = "{a, b}";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralStringNode() {
		LiteralStringNode node = new LiteralStringNode("a\\", false);
		String expected = "\"a\\\\\"";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralNullNode() {
		LiteralNullNode node = new LiteralNullNode();
		String expected = "null";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextLiteralRegExpNode() {
		LiteralRegExpNode node = new LiteralRegExpNode("/([a-z0-9]+).*/g");
		String expected = "/([a-z0-9]+).*/g";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented in Flex")
	public void testEvaluateContextLiteralXMLNode() {
	}

	@Test
	public void testEvaluateContextMemberExpressionNode() {
		MemberExpressionNode node = new MemberExpressionNode(new GetExpressionNode(new IdentifierNode("a",0)), new GetExpressionNode(new IdentifierNode("b",0)),0);
		
		String expected = "a.b";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextSetExpressionNode() {
		SetExpressionNode node = new SetExpressionNode(new IdentifierNode("a",0), new ArgumentListNode(new IdentifierNode("b",0), 0));
		
		String expected = "a = b";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextQualifiedIdentifierNodeAttrs() {
		// qualifier is usually just a bunch of attributes, like public
		QualifiedIdentifierNode node = new QualifiedIdentifierNode(
				new ArgumentListNode(
						new MemberExpressionNode(null, 
								new GetExpressionNode(new IdentifierNode("public",0)),
						        0),
						0),
				"a",
				0);
		
		String expected = "a";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextQualifiedIdentifierNodeConfig() {
		// A bit different when used in Config:: statements.
		QualifiedIdentifierNode node = new QualifiedIdentifierNode(
				new MemberExpressionNode(null, 
						new GetExpressionNode(new IdentifierNode("CONFIG",0)),
				        0),
				"FLASH",
				0);
		
		String expected = "CONFIG::FLASH";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextReturnStatementNode() {
		ReturnStatementNode node = new ReturnStatementNode(new IdentifierNode("a",0));
		
		String expected = "return a;";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextThisExpressionNode() {
		ThisExpressionNode node = new ThisExpressionNode();
		
		String expected = "this";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextTypedIdentifierNode() {
		TypedIdentifierNode node = new TypedIdentifierNode(new IdentifierNode("a", 0), new IdentifierNode("ClassName", 0), 0);
		
		String expected = "a:ClassName";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextVariableDefinitionNodeVar() {
		VariableDefinitionNode node = new VariableDefinitionNode(null, null, Tokens.VAR_TOKEN, new ListNode(null, new IdentifierNode("a",0), 0), 0);
		
		String expected = "var a;";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextVariableDefinitionNodeConst() {
		VariableDefinitionNode node = new VariableDefinitionNode(null, null, Tokens.CONST_TOKEN, new ListNode(null, new IdentifierNode("a",0), 0), 0);
		
		String expected = "const a;";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextVariableBindingNode() {
		TypedIdentifierNode tin = new TypedIdentifierNode(
				new QualifiedIdentifierNode(
						new ListNode(
								null,
								new MemberExpressionNode(
										null, 
										new GetExpressionNode(
												new IdentifierNode("public", 0))
										,0)
								,0), 
						"a", 
						0), 
				new TypeExpressionNode(
						new MemberExpressionNode(
								null,
								new GetExpressionNode(new IdentifierNode("int",0)),
								0), 
						false, 
						false), 
				0);
		VariableBindingNode node = new VariableBindingNode(null, null, 0, tin, new LiteralNumberNode("0"));
		String expected = "a:int = 0";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextImportDirectiveNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextStatementListNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextPackageNameNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextConditionalExpressionNode() {
	}

	@Test
	public void testEvaluateContextUnaryExpressionNode() {
		UnaryExpressionNode node = new UnaryExpressionNode(Tokens.NOT_TOKEN, new IdentifierNode("a",0));
		
		String expected = "!a";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextWhileStatementNode() {
		WhileStatementNode node = new WhileStatementNode(new IdentifierNode("a",0), new IdentifierNode("b",0));
		
		String expected = "while (a)\n{\n\tb\n}";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextWithStatementNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextPackageDefinitionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextPackageIdentifiersNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextFunctionSignatureNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextParameterListNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextParameterNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextExpressionStatementNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextSwitchStatementNode() {
	}

	@Test
	public void testEvaluateContextSuperStatementNode() {
		SuperStatementNode node = new SuperStatementNode(
				new CallExpressionNode(
						new SuperExpressionNode(null),
						null)
				);
		String expected = "super();";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextSuperExpressionNode() {
		SuperExpressionNode node = new SuperExpressionNode(null);
		
		String expected = "super";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextCaseLabelNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextContinueStatementNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextBreakStatementNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextIdentifierNode1() {
	}

	@Test
	public void testEvaluateContextIncrementNodeSolitaryPostfix() {
		IncrementNode node = new IncrementNode(Tokens.PLUSPLUS_TOKEN, new IdentifierNode("a",0 ), true);
		
		String expected = "a++;";
		String actual = evalAsString(new ExpressionStatementNode(node));
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextIncrementNodeSolitaryPrefix() {
		IncrementNode node = new IncrementNode(Tokens.PLUSPLUS_TOKEN, new IdentifierNode("a",0 ), false);
		
		String expected = "++a;";
		String actual = evalAsString(new ExpressionStatementNode(node));
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextIncrementNodeWithMXN() {
		//<MemberExpressionNode>
		//  <base>
		//    <ListNode>
		//      <BinaryExpressionNode>
		//        <MemberExpressionNode>
		//          <base>
		//            <MemberExpressionNode>
		//              <selector>
		//                <GetExpressionNode is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
		//                  <IdentifierNode name="_ownedModules"/>
		//                </GetExpressionNode>
		//              </selector>
		//            </MemberExpressionNode>
		//          </base>
		//          <selector>
		//            <GetExpressionNode is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="bracket">
		//              <ArgumentListNode>
		//                <MemberExpressionNode>
		//                  <selector>
		//                    <GetExpressionNode is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
		//                      <IdentifierNode name="ownedIdx"/>
		//                    </GetExpressionNode>
		//                  </selector>
		//                </MemberExpressionNode>
		//              </ArgumentListNode>
		//            </GetExpressionNode>
		//          </selector>
		//        </MemberExpressionNode>
		//        <MemberExpressionNode>
		//          <selector>
		//            <GetExpressionNode is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
		//              <IdentifierNode name="OwnedModuleQuantity"/>
		//            </GetExpressionNode>
		//          </selector>
		//        </MemberExpressionNode>
		//      </BinaryExpressionNode>
		//    </ListNode>
		//  </base>
		//  <selector>
		//    <IncrementNode mode="dot">
		//      <IdentifierNode name="Quantity"/>
		//    </IncrementNode>
		//  </selector>
		//</MemberExpressionNode>
		MemberExpressionNode node = new MemberExpressionNode(new IdentifierNode("a", 0), new IncrementNode(Tokens.PLUSPLUS_TOKEN, new IdentifierNode("b", 0), true), 0);
		
		String expected = "a.b++;";
		String actual = evalAsString(new ExpressionStatementNode(node));
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextThisExpressionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextQualifiedIdentifierNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextQualifiedExpressionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralBooleanNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralNumberNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralStringNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralNullNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralRegExpNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralXMLNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextFunctionCommonNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextParenExpressionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextParenListExpressionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralObjectNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralFieldNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralArrayNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLiteralVectorNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextSuperExpressionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextMemberExpressionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextInvokeNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextCallExpressionNode1() {
	}

	@Test
	public void testEvaluateContextDeleteExpressionNode() {
		// XSN required, or expressionPrefix won't take.
		ExpressionStatementNode node = new ExpressionStatementNode(new DeleteExpressionNode(Tokens.DELETE_TOKEN, new IdentifierNode("a", 0)));
		String expected = "delete a;";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextGetExpressionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextSetExpressionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextUnaryExpressionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextBinaryExpressionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextConditionalExpressionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextArgumentListNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextListNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextStatementListNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextEmptyElementNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextEmptyStatementNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextExpressionStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextSuperStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLabeledStatementNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextIfStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextSwitchStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextCaseLabelNode1() {
	}

	@Test
	public void testEvaluateContextDoStatementNode() {
        DoStatementNode node = new DoStatementNode(new StatementListNode(new IdentifierNode("CONTENT",0)), new IdentifierNode("CONDITION",0));
        
		String expected = "do\n{\n\tCONTENT\n} while(CONDITION)";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextWhileStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextForStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextWithStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextContinueStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextBreakStatementNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextReturnStatementNode1() {
	}

	@Test
	public void testEvaluateContextThrowStatementNode() {
		ThrowStatementNode node = new ThrowStatementNode(new IdentifierNode("a", 0));
		String expected = "throw a";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextTryStatementNode() {
        TryStatementNode node = new TryStatementNode(new StatementListNode(new IdentifierNode("CONTENT", 0)), null, null);
		String expected = "try\n{\n\tCONTENT\n}";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextCatchClauseNode() {
		TypedIdentifierNode typedID = new TypedIdentifierNode(new IdentifierNode("e",0), new IdentifierNode("Error",0), 0);
        CatchClauseNode node = new CatchClauseNode(typedID, new StatementListNode(new IdentifierNode("CONTENT", 0)));
		String expected = "catch(e:Error)\n{\n\tCONTENT\n}";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	public void testEvaluateContextFinallyClauseNode() {
        FinallyClauseNode node = new FinallyClauseNode(new StatementListNode(new IdentifierNode("CONTENT", 0)), null);
		String expected = "finally\n{\n\tCONTENT\n}";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextUseDirectiveNode() {
	}

	@Test
	public void testEvaluateContextIncludeDirectiveNode() {
//        <IncludeDirectiveNode in_this_include="true">
//          <filespec>
//            <LiteralStringNode value="C:\TiTS-Public\includes\appearance.as"/>
//          </filespec>
//        </IncludeDirectiveNode>
        IncludeDirectiveNode node = new IncludeDirectiveNode(null, new LiteralStringNode("/tmp/included.as", false), null);
		String expected = "include \"/tmp/included.as\";";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextImportDirectiveNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextAttributeListNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextVariableDefinitionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextVariableBindingNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextUntypedVariableBindingNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextTypedIdentifierNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextBinaryFunctionDefinitionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextFunctionDefinitionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextFunctionNameNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextFunctionSignatureNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextParameterNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextRestExpressionNode() {
	}

	@Test
	public void testEvaluateContextRestParameterNode() {
		// ... dTriggers ->
		//<RestParameterNode>
		//  <ParameterNode kind="-112">
		//    <identifier>
		//      <IdentifierNode name="dTriggers"/>
		//    </identifier>
		//  </ParameterNode>
		//</RestParameterNode>
		RestParameterNode node = new RestParameterNode(new ParameterNode(-112, new IdentifierNode("a", 0), null, null));		
		String expected = "... a:*";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextBinaryClassDefNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextBinaryInterfaceDefinitionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextClassDefinitionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextInterfaceDefinitionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextClassNameNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextInheritanceNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextNamespaceDefinitionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextConfigNamespaceDefinitionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextPackageDefinitionNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextPackageIdentifiersNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextPackageNameNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextProgramNode1() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextErrorNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextToObjectNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextLoadRegisterNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextStoreRegisterNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextBoxNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextCoerceNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextPragmaNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextPragmaExpressionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextParameterListNode1() {
	}

	@Test
	public void testEvaluateContextMetaDataNode() {
		MetaDataNode node = new MetaDataNode(null);
		// Yes, this is exactly as stupid as it looks. No, there are no syntax tree elements for this.
		node.setId("A");
		node.setValues(new Value[] {
			new MetaDataEvaluator.KeyValuePair("b", "1"),
		});
		String expected = "[A(b=1)]";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextDefaultXMLNamespaceNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextBinaryProgramNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextDocCommentNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextImportNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextRegisterNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextHasNextNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextTypeExpressionNode() {
	}

	@Test
	public void testEvaluateContextApplyTypeExprNode() {
		//	    <ApplyTypeExprNode>
		//        <expr>
		//          <IdentifierNode name="Vector"/>
		//        </expr>
		//        <typeArgs>
		//          <ListNode>
		//            <TypeExpressionNode is_nullable="true" nullable_annotation="false">
		//              <MemberExpressionNode>
		//                <selector>
		//                  <GetExpressionNode is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
		//                    <IdentifierNode name="SingleCharacterDisplay"/>
		//                  </GetExpressionNode>
		//                </selector>
		//              </MemberExpressionNode>
		//            </TypeExpressionNode>
		//          </ListNode>
		//        </typeArgs>
		//      </ApplyTypeExprNode>
		ApplyTypeExprNode node = new ApplyTypeExprNode(new IdentifierNode("TypeA",0), new ListNode(null, new IdentifierNode("TypeB",0), 0));
		String expected = "TypeA.<TypeB>";
		String actual = evalAsString(node);
		
		assertEquals(expected, actual);
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextUseNumericNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextUsePrecisionNode() {
	}

	@Test
	@Ignore("Not yet implemented")
	public void testEvaluateContextUseRoundingNode() {
	}

}
