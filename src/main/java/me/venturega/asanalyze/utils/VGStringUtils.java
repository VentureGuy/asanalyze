package me.venturega.asanalyze.utils;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VGStringUtils {
	Pattern REGEX_MESSAGE = Pattern.compile("\"<FONT COLOR=\\'(#[A-F0-9]{6})\\'>\\s+(.+?)\\s+([^\"]+)</FONT>\"");
	private static Pattern REG_GET_STR = Pattern.compile("\"(.*?)\"$");

	// These are translated into a class, which actually prefixes the proper
	// symbol.
	@SuppressWarnings("serial")
	public static HashMap<String, String> MessageClasses = new HashMap<String, String>() {
		{
			put("❖", "trait");
			put("☤", "medical");
			put("♟", "defense");
		}
	};

	@SuppressWarnings("serial")
	public static HashMap<String, String> ColorClasses = new HashMap<String, String>() {
		{
			put("#9933CC", "neutral");
			put("#33CCFF", "good");
			put("#FF0040", "bad");
		}
	};

	public static String markdownify(String inputstr) {
		inputstr = inputstr.replaceAll("\\\\r", "\n");
		inputstr = inputstr.replaceAll("\\*", "\\*");
		inputstr = inputstr.replaceAll("</?b>", "**");
		inputstr = inputstr.replaceAll("</?i>", "*");
		inputstr = inputstr.replaceAll("</?u>", "_");
		inputstr = inputstr.replaceAll("</?p>", "\n");
		inputstr = inputstr.replaceAll("[“”]", "\"");
		return inputstr;
		// return String.format("\"\"\"\n%s\n\"\"\"",inputstr);
	}

	public static String getStringContents(String line) {
		Matcher m = REG_GET_STR.matcher(line);
		return m.group(1);
	}

	private static String validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";

	public static String cleanName(String name) {
		String newName = "";
		for (int i = 0, size = name.length(); i < size; i++) {
			String c = name.substring(i, i + 1);
			c = c.toUpperCase();
			if (validChars.contains(c))
				newName += c;
		}
		return newName.replace(' ', '_');
	}

	public static String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String numeric = "0123456789";
	public static String alphanumeric = alpha + numeric;

	/**
	 * Converts text to Title Case.
	 * 
	 * @param name
	 * @return
	 */
	public static String capitalize(String name) {
		String o = "";
		int charsSinceLastBreak = 0;
		for (int i = 0, size = name.length(); i < size; i++) {
			String c = name.substring(i, i + 1);
			if (alphanumeric.contains(c.toUpperCase())) {
				if (charsSinceLastBreak == 0)
					o += c.toUpperCase();
				else
					o += c.toLowerCase();
				charsSinceLastBreak++;
			} else {
				charsSinceLastBreak = 0;
			}
		}
		return o;
	}
	
	public static String escapeRegex(String str) {
		return str.replaceAll("[\\<\\(\\[\\{\\\\\\^\\-\\=\\$\\!\\|\\]\\}\\)‌​\\?\\*\\+\\.\\>]", "\\\\$0");
	}

	/**
	 * Converts text to CamelCase.
	 * 
	 * @param name
	 * @return
	 */
	public static String camelcase(String name) {
		String o = "";
		int charsSinceLastBreak = 0;
		for (int i = 0, size = name.length(); i < size; i++) {
			String c = name.substring(i, i + 1);
			if (alphanumeric.contains(c.toUpperCase())) {
				if (charsSinceLastBreak == 0) // Check for uppercase.
					o += c.toUpperCase();
				else
					o += c.toLowerCase();
				charsSinceLastBreak++;
			} else {
				charsSinceLastBreak = 0;
			}
		}
		return o;
	}

	/**
	 * Decimals are printed with as few zeroes as possible, and in English
	 * locale.
	 * 
	 * @param d
	 * @return
	 */
	public static String decimalToString(double d) {
		DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		df.setMaximumFractionDigits(340); // 340 = DecimalFormat.DOUBLE_FRACTION_DIGITS
		return df.format(d);
	}

	public static String getBetween(String line, char start, char end, boolean inclusive) {
		String buf = "";
		boolean inside = false;
		char c;
		for (int i = 0; i < line.length(); i++) {
			c = line.charAt(i);
			if (!inside) {
				if (c == start) {
					if (inclusive) {
						buf += c;
					}
					inside = true;
					continue;
				}
			} else {
				if (c == end) {
					if (inclusive) {
						buf += c;
					}
					return buf;
				}
				buf += c;
			}
		}
		return "";
	}
	
	public static String escapeYAMLString(String input) {
		input = input.replace("\n", "\\n");
		input = input.replace("\r", "\\r");
		input = input.replace("\t", "\\t");
		input = input.replace("\"", "\\\"");
		input = "\""+input+"\"";
		return input;
	}
}
