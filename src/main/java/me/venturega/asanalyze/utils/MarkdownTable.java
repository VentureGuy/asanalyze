package me.venturega.asanalyze.utils;

import java.util.ArrayList;
import java.util.HashMap;

public class MarkdownTable {
	ArrayList<ArrayList<String>> rows = new ArrayList<ArrayList<String>>();
	ArrayList<String> currentRow = new ArrayList<String>();

	public MarkdownTable newRow() {
		if (!currentRow.isEmpty())
			rows.add((ArrayList<String>) currentRow.clone());
		currentRow.clear();
		return this;
	}

	public MarkdownTable addCell(String content) {
		currentRow.add(content);
		return this;
	}

	public String toMDString() {
		HashMap<Integer, Integer> colwidths = new HashMap<Integer, Integer>();
		int nrows = rows.size();
		int ncolumns = 0;
		for (int i = 0; i < nrows; i++) {
			ArrayList<String> row = rows.get(i);
			int rowlen = row.size();
			for (int j = 0; j < rowlen; j++) {
				colwidths.put(j, Math.max(row.get(j).length(), colwidths.getOrDefault(j, 0)));
			}
			ncolumns = Math.max(ncolumns, rowlen);
		}
		String o = "";
		for (int i = 0; i < nrows; i++) {
			ArrayList<String> row = rows.get(i);
			for (int j = 0, rowlen = row.size(); j < rowlen; j++) {
				if (j > 0)
					o += " | ";
				int width = colwidths.get(j);
				String content = row.get(j);
				o += String.format("%1$-" + width + "s", content);
			}
			o += "\n";
			if (i == 0) {
				for (int j = 0, rowlen = row.size(); j < rowlen; j++) {
					if (j > 0)
						o += " | ";
					int width = colwidths.get(j);
					o += String.format("%0" + width + "d", 0).replace("0", "-");
				}
				o += "\n";
			}
		}
		return o;
	}

}
