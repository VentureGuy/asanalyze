
package me.venturega.asanalyze.utils;

import java.io.PrintWriter;
import java.util.Stack;

import macromedia.asc.parser.*;
import macromedia.asc.semantics.Value;
import macromedia.asc.util.Context;
import flash.swf.tools.as3.EvaluatorAdapter;

public class CoffeePrinter extends EvaluatorAdapter {
	public class SwitchStackElement {
		public int line = 0;
		public int indent = 0;

		public SwitchStackElement(int line, int indent) {
			this.line = line;
			this.indent = indent;
		}
	}

	public IndentPrinter out;
	protected int indent;
	
	// Ugly hack.
	private Stack<SwitchStackElement> switchStack = new Stack<SwitchStackElement>();
	private int switchIndent = 0;

	public CoffeePrinter(PrintWriter out) {
		this(out, 0);
	}

	public CoffeePrinter(PrintWriter out, int indent) {
		this.out = new IndentPrinter(out);
		this.indent = indent;
	}

	public CoffeePrinter(IndentPrinter w) {
		this.out = w;
		this.indent = w.indent;
	}

	@Override
	public Value evaluate(Context cx, AttributeListNode node) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			n.evaluate(cx, this);
			out.print(" ");
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ArgumentListNode node) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			n.evaluate(cx, this);
			if ((i + 1) < size) {
				out.print(", ");
			}
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, BinaryExpressionNode node) {
		if (node.lhs != null) {
			node.lhs.evaluate(cx, this);
		}

		op(node.op);

		if (node.rhs != null) {
			node.rhs.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, CallExpressionNode node) {
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.print("(");
		if (node.args != null) {
			node.args.evaluate(cx, this);
		}
		out.print(")");
		return null;
	}

	@Override
	public Value evaluate(Context cx, PackageDefinitionNode node) {
		return null;
	}

	@Override
	public Value evaluate(Context cx, ClassDefinitionNode node) {
		out.beginLine().print("class ");

		if ((node.name != null) && (node.name.name != null)) {
			node.name.evaluate(cx, this);
		} else if ((node.cframe != null) && (node.cframe.builder != null)) {
			//out.print("QNAME[");
			out.print(node.cframe.builder.classname.toString());
			//out.print("]");
		} else {
			assert false : "No class name found for ClassDefinitionNode";
		}

		if (node.baseclass != null) {
			out.print(" extends ");
			node.baseclass.evaluate(cx, this);
		}
		if (node.interfaces != null) {
			out.beginLine();
			out.print("# implements ");
			node.interfaces.evaluate(cx, this);
			out.println("");
		}
		out.beginLine().indent();
		{
			if (node.statements != null) {
				node.statements.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	@Override
	public Value evaluate(Context cx, ForStatementNode node) {

		// old: for(INIT;TEST;INCREMENT) {...}

		// out.print("for (");
		out.println("# Initializer");
		if (node.initialize != null) {
			node.initialize.evaluate(cx, this);
		}
		out.println("");
		out.print("while ");
		if (node.test != null) {
			node.test.evaluate(cx, this);
		}
		out.indent();
		{
			out.println("# Increment");
			if (node.increment != null) {
				node.increment.evaluate(cx, this);
			}
			out.println("");
			out.println("# Statements");
			if (node.statement != null) {
				node.statement.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	@Override
	public Value evaluate(Context cx, FunctionCommonNode node) {
		out.beginLine();
		out.print("(");
		if (node.signature != null) {
			node.signature.evaluate(cx, this);
		}
		out.println(") =>");
		out.indent();
		{
			indent++;
			if (node.body != null) {
				node.body.evaluate(cx, this);
			}
		}
		out.dedent();
		out.println("");
		return null;
	}

	@Override
	public Value evaluate(Context cx, FunctionDefinitionNode node) {
		out.println("###");
		if (node.attrs != null) {
			node.attrs.evaluate(cx, this);
		}
		out.beginLine().println("###");
		if (node.name != null) {
			node.name.evaluate(cx, this);
		}
		out.print(": ");
		if (node.fexpr != null) {
			node.fexpr.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, GetExpressionNode node) {
		if (node.expr != null) {
			if (node.expr instanceof ArgumentListNode) {
				out.print("[");
				node.expr.evaluate(cx, this);
				out.print("]");
			} else {
				node.expr.evaluate(cx, this);
			}
		}

		return null;
	}

	@Override
	public Value evaluate(Context cx, IdentifierNode node) {
		out.print(node.name);
		return null;
	}

	@Override
	public Value evaluate(Context cx, IfStatementNode node) {
		out.beginLine().print("if ");
		if (node.condition != null) {
			node.condition.evaluate(cx, this);
		}
		out.println("");
		out.indent();
		{
			if (node.thenactions != null) {
				node.thenactions.evaluate(cx, this);
			}
		}
		out.dedent();
		if (node.elseactions != null) {
			out.beginLine().println("else").indent();
			{
				node.elseactions.evaluate(cx, this);
			}
			out.dedent();
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, InterfaceDefinitionNode node) {
		out.beginLine();
		out.print("class ");

		if ((node.name != null) && (node.name.name != null)) {
			node.name.evaluate(cx, this);
		} else if ((node.cframe != null) && (node.cframe.builder != null)) {
			out.print(node.cframe.builder.classname.toString());
		} else {
			assert false : "No class name found for ClassDefinitionNode";
		}

		if (node.baseclass != null) {
			out.print(" extends ");
			node.baseclass.evaluate(cx, this);
		}
		out.println("").indent();
		{
			if (node.statements != null) {
				node.statements.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	@Override
	public Value evaluate(Context cx, ListNode node) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			n.evaluate(cx, this);
			if ((i + 1) < size) {
				out.print(", ");
			}
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralArrayNode node) {
		out.print("[");
		super.evaluate(cx, node);
		out.print("]");
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralVectorNode node) {
		// out.print("/*new<");

		// evaluate(cx, node.type);

		// out.print(">*/");
		out.print("[");
		super.evaluate(cx, node.elementlist);
		out.print("]");
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralBooleanNode node) {
		out.print(node.value ? "true" : "false");
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralFieldNode node) {
		if (node.name != null) {
			node.name.evaluate(cx, this);
		}

		out.print(": ");

		if (node.value != null) {
			node.value.evaluate(cx, this);
		}

		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralNumberNode node) {
		out.print(node.value);
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralObjectNode node) {
		out.print("{");
		super.evaluate(cx, node);
		out.print("}");
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralStringNode node) {
		out.print("\"" + node.value.replace("\t", "\\t").replace("\n", "\\n").replace("\r", "\\r") + "\"");
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralNullNode node) {
		out.print("null");
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralRegExpNode node) {
		out.print(node.value);
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralXMLNode node) {
		assert false : "Not implemented yet.";
		return null;
	}

	@Override
	public Value evaluate(Context cx, MemberExpressionNode node) {
		if (node.base != null) {
			node.base.evaluate(cx, this);
			if ((node.selector != null)) {
				if ((node.selector instanceof GetExpressionNode)
						&& (!(((GetExpressionNode) node.selector).expr instanceof ArgumentListNode))) {
					out.print(".");
				} else if ((node.selector instanceof SetExpressionNode)
						&& (!(((SetExpressionNode) node.selector).expr instanceof ArgumentListNode))) {
					out.print(".");
				} else if (node.selector instanceof CallExpressionNode) {
					out.print(".");
				}
			}
		}

		if (node.selector != null) {
			node.selector.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ProgramNode node) {
		for (int i = 0; i < node.imports.size(); i++) {
			out.beginLine();
			out.print("# import ");
			out.print(node.imports.get(i).toString());
			out.println(";");
		}

		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, QualifiedIdentifierNode node) {
		/*
		 * This returns shit like "public", which we don't want.
		 * out.print("QualifiedIdentifierNode("); if (node.qualifier != null) {
		 * out.print("qualifier=["); node.qualifier.evaluate(cx, this);
		 * out.print("]"); }
		 */
		evaluate(cx, (IdentifierNode) node);
		// out.print(")");
		return null;
	}

	@Override
	public Value evaluate(Context cx, ReturnStatementNode node) {
		out.beginLine();
		out.print("return ");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.println("");
		return null;
	}

	@Override
	public Value evaluate(Context cx, ThisExpressionNode node) {
		out.print("@");
		return null;
	}

	@Override
	public Value evaluate(Context cx, TypedIdentifierNode node) {
		/*
		 * if (node.type != null) { out.print(":"); node.type.evaluate(cx,
		 * this); }
		 */
		if (node.identifier != null) {
			node.identifier.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, VariableDefinitionNode node) {
		out.beginLine();
		/*
		 * if (node.attrs != null) { node.attrs.evaluate(cx, this); }
		 */
		// out.print("var ");
		if (node.list != null) {
			node.list.evaluate(cx, this);
		}
		// out.println(";");
		return null;
	}

	@Override
	public Value evaluate(Context cx, WhileStatementNode node) {
		out.beginLine();
		out.print("while ");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.println("").indent();
		{
			if (node.statement != null) {
				node.statement.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	@Override
	public Value evaluate(Context cx, WithStatementNode node) {
		out.beginLine();
		out.print("with ");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.println("").indent();
		{
			if (node.statement != null) {
				node.statement.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	private void op(int op) {
		switch (op) {
		case Tokens.NOTEQUALS_TOKEN: {
			out.print(" != ");
			break;
		}
		case Tokens.STRICTNOTEQUALS_TOKEN: {
			out.print(" !== ");
			break;
		}
		case Tokens.LESSTHAN_TOKEN: {
			out.print(" < ");
			break;
		}
		case Tokens.LESSTHANOREQUALS_TOKEN: {
			out.print(" <= ");
			break;
		}
		case Tokens.EQUALS_TOKEN: {
			out.print(" == ");
			break;
		}
		case Tokens.STRICTEQUALS_TOKEN: {
			out.print(" === ");
			break;
		}
		case Tokens.GREATERTHAN_TOKEN: {
			out.print(" > ");
			break;
		}
		case Tokens.GREATERTHANOREQUALS_TOKEN: {
			out.print(" >= ");
			break;
		}
		case Tokens.MULTASSIGN_TOKEN: {
			out.print(" *= ");
			break;
		}
		case Tokens.MULT_TOKEN: {
			out.print(" * ");
			break;
		}
		case Tokens.DIVASSIGN_TOKEN: {
			out.print(" /= ");
			break;
		}
		case Tokens.DIV_TOKEN: {
			out.print(" / ");
			break;
		}
		case Tokens.MODULUSASSIGN_TOKEN: {
			out.print(" %= ");
			break;
		}
		case Tokens.MODULUS_TOKEN: {
			out.print(" % ");
			break;
		}
		case Tokens.PLUSASSIGN_TOKEN: {
			out.print(" += ");
			break;
		}
		case Tokens.PLUS_TOKEN: {
			out.print(" + ");
			break;
		}
		case Tokens.MINUSASSIGN_TOKEN: {
			out.print(" -= ");
			break;
		}
		case Tokens.MINUS_TOKEN: {
			out.print(" - ");
			break;
		}
		case Tokens.LEFTSHIFTASSIGN_TOKEN: {
			out.print(" <<= ");
			break;
		}
		case Tokens.LEFTSHIFT_TOKEN: {
			out.print(" << ");
			break;
		}
		case Tokens.RIGHTSHIFTASSIGN_TOKEN: {
			out.print(" >>= ");
			break;
		}
		case Tokens.RIGHTSHIFT_TOKEN: {
			out.print(" >> ");
			break;
		}
		case Tokens.UNSIGNEDRIGHTSHIFTASSIGN_TOKEN: {
			out.print(" >>>= ");
			break;
		}
		case Tokens.UNSIGNEDRIGHTSHIFT_TOKEN: {
			out.print(" >>> ");
			break;
		}
		case Tokens.BITWISEANDASSIGN_TOKEN: {
			out.print(" &= ");
			break;
		}
		case Tokens.BITWISEAND_TOKEN: {
			out.print(" & ");
			break;
		}
		case Tokens.BITWISEXORASSIGN_TOKEN: {
			out.print(" |= ");
			break;
		}
		case Tokens.BITWISEXOR_TOKEN: {
			out.print(" ^ ");
			break;
		}
		case Tokens.BITWISEORASSIGN_TOKEN: {
			out.print(" ^= ");
			break;
		}
		case Tokens.BITWISEOR_TOKEN: {
			out.print(" | ");
			break;
		}
		case Tokens.LOGICALANDASSIGN_TOKEN: {
			out.print(" &&= ");
			break;
		}
		case Tokens.LOGICALAND_TOKEN: {
			out.print(" and ");
			break;
		}
		case Tokens.LOGICALXORASSIGN_TOKEN: {
			out.print(" ^^= ");
			break;
		}
		case Tokens.LOGICALXOR_TOKEN: {
			out.print(" ^^ ");
			break;
		}
		case Tokens.LOGICALORASSIGN_TOKEN: {
			out.print(" ||= ");
			break;
		}
		case Tokens.LOGICALOR_TOKEN: {
			out.print(" or ");
			break;
		}
		default: {
			assert false : "Unhandled operation, " + op + ".";
		}
		}
	}

	/*
	 * @Override public Value evaluate(Context arg0, Node arg1) { }
	 */

	@Override
	public Value evaluate(Context cx, IncrementNode node) {
		if (!node.isPostfix)
			out.print("++ ");
		node.expr.evaluate(cx, this);
		if (node.isPostfix)
			out.print(" ++");
		return null;
	}

	@Override
	public Value evaluate(Context cx, DeleteExpressionNode node) {
		// del ...
		out.print("del ");
		node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, InvokeNode node) {
		// ????
		out.print("<InvokeNode>");
		if (node.args != null) {
			node.args.evaluate(cx, this);
		}
		out.print("</InvokeNode>");
		return null;
	}

	@Override
	public Value evaluate(Context cx, QualifiedExpressionNode node) {
		// ????
		out.print("<QualifiedExpressionNode>");
		out.print(node.name);
		if (node.qualifier != null) {
			out.print("<qualifier>");
			node.qualifier.evaluate(cx, this);
			out.print("</qualifier>");
		}
		if (node.expr != null) {
			out.print("<expr>");
			node.expr.evaluate(cx, this);
			out.print("</expr>");
		}
		out.print("</QualifiedExpressionNode>");
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParenExpressionNode node) {
		out.print("(");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.print(")");
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParenListExpressionNode node) {
		out.print("(");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.print(")");
		return null;
	}

	@Override
	public Value evaluate(Context cx, SuperExpressionNode node) {
		out.beginLine().print("super");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, SuperStatementNode node) {
		out.beginLine().print("super");
		out.print("(");
		if (node.call != null && node.call.args != null) {
			node.call.args.evaluate(cx, this);
		}
		out.print(")");
		out.println("");
		return null;
	}

	@Override
	public Value evaluate(Context cx, SetExpressionNode node) {
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.print(" = "); // TODO: idfk
		if (node.args != null) {
			node.args.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ApplyTypeExprNode node) {
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}

		if (node.typeArgs != null) {
			node.typeArgs.evaluate(cx, this);
		}

		return null;
	}

	@Override
	public Value evaluate(Context cx, UnaryExpressionNode node) {
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ConditionalExpressionNode node) {
		out.print("if ");
		if (node.condition != null) {
			node.condition.evaluate(cx, this);
		}
		out.print(" then ");
		if (node.thenexpr != null) {
			node.thenexpr.evaluate(cx, this);
		}
		out.print(" else ");
		if (node.elseexpr != null) {
			node.elseexpr.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, StatementListNode node) {
		// Reevaluate the size for each iteration, because Nodes can
		// be added to "items" (See the NodeMagic.addImport() call in
		// flex2.compiler.as3.SyntaxTreeEvaluator.processResourceBundle())
		// and if the last Node is an IncludeDirectiveNode, we need to
		// be sure to evaluate it, so that in_this_include is turned
		// off.

		// for (Node n : node.items)
		for (int i = 0; i < node.items.size(); i++) {
			Node n = node.items.get(i);
			if (n != null) {
				n.evaluate(cx, this);
			}
		}

		return null;
	}

	@Override
	public Value evaluate(Context arg0, EmptyElementNode arg1) {
		out.print("`/* EmptyElementNode */`");
		return null;
	}

	@Override
	public Value evaluate(Context arg0, EmptyStatementNode arg1) {
		out.print("`/* EmptyStatementNode */`");
		return null;
	}

	@Override
	public Value evaluate(Context cx, ExpressionStatementNode node) {
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		// out.println(" # EXPRESSIONSTATEMENTNODE");
		out.println("");
		return null;
	}

	@Override
	public Value evaluate(Context cx, LabeledStatementNode node) {
		if (node.label != null) {
			node.label.evaluate(cx, this);
		}
		out.println(":").indent();
		{
			if (node.statement != null) {
				node.statement.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	@Override

	public Value evaluate(Context cx, SwitchStatementNode node) {
		out.print("switch ");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.println("").indent();
		switchStack.push(new SwitchStackElement(0, out.indent));
		{
			if (node.statements != null) {
				node.statements.evaluate(cx, this);
			}
		}
		SwitchStackElement e = switchStack.pop();
		out.indent=e.indent-1;
		return null;
	}

	@Override
	public Value evaluate(Context cx, CaseLabelNode node) {
		out.indent = switchStack.peek().indent;
		if (node.label != null) {
			out.print("when ");
			node.label.evaluate(cx, this);
		} else {
			out.print("default");
		}
		out.println("");
		out.indent = switchStack.peek().indent+1;
		return null;
	}

	@Override
	public Value evaluate(Context cx, DoStatementNode node) {
		out.print("do ");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.println("").indent();
		{
			if (node.statements != null) {
				node.statements.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	@Override
	public Value evaluate(Context cx, ContinueStatementNode node) {
		out.print("continue");
		if (node.id != null) {
			out.print(" ");
			node.id.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, BreakStatementNode node) {
		out.print("break");
		if (node.id != null) {
			out.print(" ");
			node.id.evaluate(cx, this);
		}
		out.println("");
		
		return null;
	}

	@Override
	public Value evaluate(Context cx, ThrowStatementNode node) {
		out.print("throw ");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.println("");
		return null;
	}

	@Override
	public Value evaluate(Context cx, TryStatementNode node) {
		out.println("try").indent();
		{
			if (node.tryblock != null) {
				node.tryblock.evaluate(cx, this);
			}
		}
		out.dedent();
		if (node.catchlist != null) {
			node.catchlist.evaluate(cx, this);
		}
		out.println("finally").indent();
		{
			if (node.finallyblock != null) {
				node.finallyblock.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	@Override
	public Value evaluate(Context cx, CatchClauseNode node) {
		out.print("catch ");
		if (node.parameter != null) {
			node.parameter.evaluate(cx, this);
		}
		out.indent();
		{
			if (node.statements != null) {
				node.statements.evaluate(cx, this);
			}
		}
		out.dedent();
		return null;
	}

	@Override
	public Value evaluate(Context cx, FinallyClauseNode node) {
		if (node.statements != null) {
			node.statements.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context arg0, UseDirectiveNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, IncludeDirectiveNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, ImportNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, MetaDataNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, DocCommentNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, ImportDirectiveNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, VariableBindingNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, UntypedVariableBindingNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, TypeExpressionNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, BinaryFunctionDefinitionNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context cx, FunctionNameNode node) {
		if (node.identifier != null) {
			node.identifier.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, FunctionSignatureNode node) {
		// Function parameter (parameter:result)
		if (node.result != null) {
			out.print("`/* ");
			node.result.evaluate(cx, this);
			out.print(" */`");
		}

		if (node.parameter != null) {
			node.parameter.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParameterNode node) {
		if (node.type != null) {
			node.type.evaluate(cx, this);
		}
		out.print(": ");
		if (node.identifier != null) {
			node.identifier.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context arg0, ParameterListNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, RestExpressionNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, RestParameterNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, BinaryClassDefNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, BinaryInterfaceDefinitionNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, ClassNameNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, InheritanceNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, NamespaceDefinitionNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, ConfigNamespaceDefinitionNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, PackageIdentifiersNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, PackageNameNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, BinaryProgramNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, ErrorNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, ToObjectNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, LoadRegisterNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, StoreRegisterNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, RegisterNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, HasNextNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, BoxNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, CoerceNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, PragmaNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, UsePrecisionNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, UseNumericNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, UseRoundingNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, PragmaExpressionNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value evaluate(Context arg0, DefaultXMLNamespaceNode arg1) {
		// TODO Auto-generated method stub
		return null;
	}
}
