package me.venturega.asanalyze.utils;

import java.util.ArrayList;

import macromedia.asc.parser.*;
import macromedia.asc.util.Context;

public class ASTTypeUtils {

	public static Node buildDefaultInitializerFor(Context cx, Node node) {
		NodeFactory factory = cx.getNodeFactory();
		if(node instanceof LiteralStringNode) {
			return factory.literalString("");
		}
		if(node instanceof LiteralNumberNode) {
			return factory.literalNumber("0");
		}
		if(node instanceof LiteralArrayNode) {
			return factory.literalArray(null);
		}
		if(node instanceof LiteralBooleanNode) {
			return factory.literalBoolean(false);
		}
		return factory.literalNull();
	}

	public static Object simplify(Node node) {
		if(node instanceof LiteralStringNode) {
			return ((LiteralStringNode)node).value;
		}
		if(node instanceof LiteralNumberNode) {
			return ((LiteralNumberNode)node).value;
		}
		if(node instanceof LiteralArrayNode) {
			return simplify(((LiteralArrayNode)node).elementlist);
		}
		if(node instanceof ArgumentListNode) {
			ArrayList<Object> arr = new ArrayList<Object>();
			for(Node n : ((ArgumentListNode)node).items){
				arr.add(simplify(n));
			}
		}
		if(node instanceof LiteralBooleanNode) {
			return ((LiteralBooleanNode)node).value;
		}
		return null;
	}

}
