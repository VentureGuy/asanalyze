package me.venturega.asanalyze.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.yaml.snakeyaml.Yaml;

import macromedia.asc.parser.ClassDefinitionNode;
import macromedia.asc.parser.FunctionDefinitionNode;
import macromedia.asc.parser.Node;
import macromedia.asc.parser.ParameterNode;
import macromedia.asc.util.Context;
import macromedia.asc.util.ObjectList;
import me.venturega.asanalyze.codegen.CoffeeClass;
import me.venturega.asanalyze.codegen.CoffeeMethod;
import me.venturega.asanalyze.evaluators.ParentizerEvaluator;
import me.venturega.asanalyze.evaluators.ReplaceEvaluator;
import me.venturega.asanalyze.evaluators.SearchEvaluator;
import me.venturega.asanalyze.utils.FileUtils.AutogenFileHandles;

public class AstAnalyzer {
	protected Context cx;
	public ParentizerEvaluator parents;
	protected List<String> writtenFiles = new ArrayList<String>();

	public Node FindFirst(Class<? extends Node> T, ObjectList<Node> blk, int startPos) {
		Node stmt = null;
		for (int i = startPos, size = blk.size(); i < size; i++) {
			stmt = blk.get(i);
			if (stmt.getClass().isAssignableFrom(T)) {
				return stmt;
			}
		}
		return null;
	}

	public ArrayList<Node> FindRecursive(Node N, SearchEvaluator.Locator test) {
		SearchEvaluator searcher = new SearchEvaluator(test);
		N.evaluate(cx, searcher);
		return searcher.found;
	}
	
	public void TransformRecursive(Node N, Function<Node, Node> transformation) {
		ReplaceEvaluator searcher = new ReplaceEvaluator(transformation);
		N.evaluate(cx, searcher);
	}

	public void ProcessMethod(String clsName, String fncName, ClassDefinitionNode cls, FunctionDefinitionNode fnc) {
	}

	public void setContext(Context cx) {
		this.cx = cx;
	}
	
	public void setParentData(ParentizerEvaluator parents) {
		this.parents=parents;
	}

	public void PostProcess() {
		// TODO Auto-generated method stub

	}
	
	public void dumpFunctionTo(FunctionDefinitionNode fnc, String functionName, String outfile, String nameMapFile) {
		HashMap<String,String> nameMappings = loadNameMappings(nameMapFile);
		PrintWriter pw=null;
		try {
			System.out.println(String.format("Writing %s...", outfile));
			AutogenFileHandles afh = FileUtils.BeginAutogenFile(outfile,false);
			pw = afh.writer;
			IndentPrinter w = new IndentPrinter(pw);
			//Context cx = new Context(new ContextStatics());
			CoffeeClass cls = new CoffeeClass();
			cls.name = "EmptyClass";
			cls.documentation="";

			CoffeeMethod method = new CoffeeMethod(functionName);
			cls.methods.put(functionName, method);
			method.documentation="";
			for(int i =0,size=fnc.fexpr.signature.parameter.items.size();i<size;i++){
				ParameterNode param = fnc.fexpr.signature.parameter.items.get(i);
				method.addArgument(param.identifier.name);
			}
			
			cls.nameMappings = nameMappings;
			method.body=fnc.fexpr.body;
			cls.fixIdentifiers();
			method.WriteIndented(method.getContext(), w);
			writtenFiles.add(new File(afh.filename).getAbsolutePath());
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		} finally {
			if (pw != null)
				pw.close();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected HashMap<String, String> loadNameMappings(String filename) {
		File cfgFile = new File(filename);
		HashMap<String, String> nameMappings = new HashMap<String,String>();
		if (cfgFile.isFile()) {
			Yaml yml = new Yaml();
			InputStreamReader input = null;
			try {
				input = new InputStreamReader(new FileInputStream(filename), "UTF8");
				Map m = (Map) yml.load(input);
				//System.out.println(m != null ? m.getClass().getName() : "null");
				for (Map.Entry<String, String> e : ((Map<String, String>) m.getOrDefault("this",new HashMap<String,String>())).entrySet()) {
					nameMappings.put(e.getKey(), "@" + e.getValue());
				}
				for (Map.Entry<String, String> e : ((Map<String, String>) m.getOrDefault("global",new HashMap<String,String>())).entrySet()) {
					nameMappings.put(e.getKey(), e.getValue());
				}

			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return nameMappings;
	}

}
