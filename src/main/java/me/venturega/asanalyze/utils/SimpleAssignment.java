package me.venturega.asanalyze.utils;

import macromedia.asc.parser.*;

public class SimpleAssignment {

	public enum EPart {
		LHS, RHS
	}

	public String rhs = "";
	public String lhs = "";
	private boolean borked=false;

	// ([a-z\.]+)\.evaluate\(cx, this\);
	// simplify($1, part);

	private void _simplify(SetExpressionNode node, EPart part) {
		if (node.expr != null) {
			simplify(node.expr, EPart.LHS);
		}
		if (node.args != null) {
			//simplify(node.args, EPart.RHS);
			append(EPart.RHS, ASTUtils.Node2String(node.args));
		} else {
			append(EPart.RHS, node.base.toString());
		}
	}

	public void simplify(Node node, EPart part) {
		if (node instanceof SetExpressionNode)
			_simplify((SetExpressionNode) node, part);
		else if (node instanceof GetExpressionNode)
			_simplify((GetExpressionNode) node, part);
		else if (node instanceof MemberExpressionNode)
			_simplify((MemberExpressionNode) node, part);
		else if (node instanceof ArgumentListNode)
			_simplify((ArgumentListNode) node, part);
		else if (node instanceof ListNode)
			_simplify((ListNode) node, part);
		else if (node instanceof CallExpressionNode)
			_simplify((CallExpressionNode) node, part);
		else if (node instanceof LiteralNumberNode)
			_simplify((LiteralNumberNode) node, part);
		else if (node instanceof LiteralStringNode)
			_simplify((LiteralStringNode) node, part);
		else if (node instanceof LiteralBooleanNode)
			_simplify((LiteralBooleanNode) node, part);
		else if (node instanceof IdentifierNode)
			_simplify((IdentifierNode) node, part);
		else if (node instanceof LiteralArrayNode)
			_simplify((LiteralArrayNode)node, part);
		else {
			System.out.println(String.format("WARN - SimpleAssignment: Unknown type %s on %s!", node.getClass().getCanonicalName(), part == EPart.LHS ? "LHS" : "RHS"));
			append(part, ASTUtils.Node2String(node));
			borked=true;
		}
	}

	private void _simplify(LiteralArrayNode node, EPart part) {
		append(part, "[");
		simplify(node.elementlist, part);
		append(part, "]");
	}

	private void _simplify(LiteralBooleanNode node, EPart part) {
		append(part, node.value ? "true" : "false");
	}

	private void _simplify(ListNode node, EPart part) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			simplify(n, part);
			if ((i + 1) < size) {
				append(part, ", ");
			}
		}
	}

	private void _simplify(IdentifierNode node, EPart part) {
		append(part, node.toIdentifierString());
	}

	private void _simplify(LiteralNumberNode node, EPart part) {
		append(part, node.value);
	}

	private void _simplify(LiteralStringNode node, EPart part) {
		append(part, "\"");
		append(part, node.value);
		append(part, "\"");
	}

	private void _simplify(CallExpressionNode node, EPart part) {
		if (node.expr != null) {
			simplify(node.expr, part);
		}
		append(part, "(");
		if (node.args != null) {
			simplify(node.args, part);
		}
		append(part, ")");
	}

	private void _simplify(ArgumentListNode node, EPart part) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			simplify(n, part);
			if ((i + 1) < size) {
				append(part, ", ");
			}
		}
	}

	private void _simplify(MemberExpressionNode node, EPart part) {
		if (node.base != null) {
			simplify(node.base, part);
			if ((node.selector != null)) {
				if ((node.selector instanceof GetExpressionNode)
						&& (!(((GetExpressionNode) node.selector).expr instanceof ArgumentListNode))) {
					append(part, ".");
				} else if ((node.selector instanceof SetExpressionNode)
						&& (!(((SetExpressionNode) node.selector).expr instanceof ArgumentListNode))) {
					append(part, ".");
				} else if (node.selector instanceof CallExpressionNode) {
					append(part, ".");
				}
			}
		}

		if (node.selector != null) {
			simplify(node.selector, part);
		}
	}

	private void append(EPart part, String string) {
		switch (part) {
		case LHS:
			lhs += string;
			break;
		case RHS:
			rhs += string;
			break;
		default:
			System.out.println(String.format("append: Invalid value %s",part.toString()));
			break;
		}
	}

	private void _simplify(GetExpressionNode node, EPart part) {
		if (node.expr != null) {
			if (node.expr instanceof ArgumentListNode) {
				append(part, "[");
				simplify(node.expr, part);
				append(part, "]");
			} else {
				simplify(node.expr, part);
			}
		}
	}

	public void simplify(ExpressionStatementNode expression) {
		simplify(expression.expr, EPart.LHS);
		if(borked) {
			System.out.println("[LHS] "+lhs);
			System.out.println("[RHS] "+rhs);
		}
	}
}
