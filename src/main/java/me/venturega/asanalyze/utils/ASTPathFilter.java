package me.venturega.asanalyze.utils;

import macromedia.asc.parser.*;
import macromedia.asc.util.Context;

import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

public class ASTPathFilter {
	private static HashMap<String,Class<? extends Node>> validClasses = new HashMap<String,Class<? extends Node>>(){
		{
			put("ExpressionStatementNode",ExpressionStatementNode.class);
			put("ListNode",ListNode.class);
		}
	};
	public ArrayList<Function<Node, Node>> elements = new ArrayList<Function<Node, Node>>();
	public Node current;
	public Context cx;
	public String path = "";
	public boolean printFails = false;
	public boolean printMatches = true;
	public int level = 0;
	public int minFailLevel = 0;

	public enum EPathParserState {
		NONE,
		IN_ATTRIBUTE,
		IN_TYPE,
		IN_INDEX,
	}
	public ASTPathFilter() {
	}

	public ASTPathFilter(String expression) throws Exception {
		// /ExpressionStatementNode.expr/ListNode[0]/
		String buf="";
		String c = "";
		String controlChars="[./";
		String identifierChars=VGStringUtils.alphanumeric+"_";
		EPathParserState state = EPathParserState.NONE;
		for(int i =0,size=expression.length();i<size;i++){
			c = expression.substring(i, i+1); 
			switch(state) {
			case NONE:
				switch(c) {
				case ".": state = EPathParserState.IN_ATTRIBUTE; break;
				case "/": state = EPathParserState.IN_TYPE; break;
				case "[": state = EPathParserState.IN_INDEX; break;
				default:
					throw new Exception(String.format("Unknown start character %s",c));
				}
				break;
			case IN_ATTRIBUTE:
				if(identifierChars.contains(c.toLowerCase())) {
					buf += c;
				} else if (controlChars.contains(c)) {
					this.selectField(buf);
					buf="";
					state=EPathParserState.NONE;
				}
				break;
			case IN_TYPE:
				if(identifierChars.contains(c.toLowerCase())) {
					buf += c;
				} else if (controlChars.contains(c)) {
					this.expect(buf);
					buf="";
					state=EPathParserState.NONE;
				}
				break;
			case IN_INDEX:
				if(c=="]") {
					this.selectItem(Integer.parseInt(buf));
					buf="";
					state=EPathParserState.NONE;
				}
				else if(VGStringUtils.numeric.contains(c)) {
					buf += c;
				}
				break;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private ASTPathFilter expect(String name) {
		try {
			expect((Class<? extends Node>)Class.forName(name));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return this;
	}

	public ASTPathFilter addFilterElement(Function<Node, Node> element) {
		this.elements.add(element);
		return this;
	}

	public Node Check(Context cx, Node start) {
		this.cx = cx;
		this.current = start;
		this.path = "root";
		level = 0;
		for (Function<Node, Node> nfnc : elements) {
			current = nfnc.apply(this.current);
			if (current == null)
				return null;
			level++;
		}
		return current;
	}

	// private void blah() {
	// String path = "act[x]";
	// if (!(ni instanceof ExpressionStatementNode)) {
	// System.out.println(path + "=" + ni.getClass().getName());
	// continue;
	// }
	// n = ((ExpressionStatementNode) ni).expr;
	// path += ".expr";
	// if (!(n instanceof ListNode)) {
	// System.out.println(path + "=" + n.getClass().getName());
	// continue;
	// }
	// n = ((ListNode) n).items.get(0);
	// path += ".expr[0]";
	// if (!(n instanceof MemberExpressionNode)) {
	// System.out.println(path + "=" + n.getClass().getName());
	// continue;
	// }
	// n = ((MemberExpressionNode) n).selector;
	// path += ".selector";
	// if (!(n instanceof SetExpressionNode)) {
	// System.out.println(path + "=" + n.getClass().getName());
	// continue;
	// }
	// n = ((SetExpressionNode) n).args.items.get(0);
	// path += ".args[0]";
	// if (!(n instanceof MemberExpressionNode)) {
	// System.out.println(path + "=" + n.getClass().getName());
	// continue;
	// }
	// n = ((MemberExpressionNode) n).selector;
	// path += ".selector";
	// if (!(n instanceof SetExpressionNode)) {
	// System.out.println(path + "=" + n.getClass().getName());
	// continue;
	// }
	// n = ((SetExpressionNode) n).args.items.get(0);
	// path += ".args[0]";
	// if (!(n instanceof LiteralArrayNode)) {
	// System.out.println(path + "=" + n.getClass().getName());
	// continue;
	// }
	// }

	private ASTPathFilter selectField(final String _attrName) {
		addFilterElement(new Function<Node, Node>() {
			public Node apply(Node n) {
				int eID = -1;
				String attrName = _attrName; // Cheating~
				if (attrName.endsWith("]")) {
					int arrayStartIndex = attrName.indexOf("[");
					if (arrayStartIndex == -1) {
						return null;
					}
					String newName = attrName.substring(0, arrayStartIndex - 1);
					eID = Integer.parseInt(attrName.substring(arrayStartIndex + 1, attrName.length() - 2));
					attrName = newName;
				}
				path += "." + attrName;
				Field f;
				try {
					f = n.getClass().getField(attrName);
				} catch (NoSuchFieldException e1) {
					if(printFails && level >= minFailLevel)
						System.out.println(String.format("%s: NoSuchFieldException: %s", path, attrName));
					return null;
				} catch (SecurityException e1) {
					if(printFails && level >= minFailLevel)
						System.out.println(String.format("%s: SecurityException: %s", path, attrName));
					return null;
				}
				if (f == null) {
					return null;
				}
				Object newVal;
				try {
					newVal = f.get(n);
				} catch (IllegalArgumentException e) {
					if(printFails && level >= minFailLevel)
						System.out.println(String.format("%s: IllegalArgumentException: %s", path, attrName));
					return null;
				} catch (IllegalAccessException e) {
					if(printFails && level >= minFailLevel)
						System.out.println(String.format("%s: IllegalAccessException: %s", path, attrName));
					return null;
				}
				if (newVal == null) {
					if(printFails && level >= minFailLevel)
						System.out.println(String.format("%s: %s == null", path, attrName));
					return null;}
				if (eID > -1) {
					if (newVal instanceof ArgumentListNode) {
						newVal = ((ArgumentListNode) newVal).items.get(eID);
					} else if (newVal instanceof ListNode) {
						newVal = ((ListNode) newVal).items.get(eID);
					} else {
						return null;
					}
				}
				if (!(newVal instanceof Node))
					return null;
				return (Node) newVal;
			}
		});
		return this;
	}

	public ASTPathFilter expect(final Class<? extends Node> cls) {
		addFilterElement(new Function<Node, Node>() {
			public Node apply(Node n) {
				if (!cls.isInstance(n)) {
					if (printFails && level >= minFailLevel)
						System.out.println(String.format("%s: %s != %s", path, n.getClass().getName(), cls.getName()));
					return null;
				}
				if (printMatches)
					System.out.println(String.format("%s: %s == %s", path, n.getClass().getName(), cls.getName()));
				return n;
			}
		});
		return this;
	}

	private ASTPathFilter selectPath(String path) {
		if (path == "")
			return this;
		String type = path.substring(0, 1);
		switch (type) {
		case ".":
			return selectField(path.substring(1));
		case "[":
			return selectItem(path.substring(1, path.length() - 2));
		}

		return this;
	}

	public ASTPathFilter selectItem(String index) {
		return selectItem(Integer.parseInt(index));
	}

	public ASTPathFilter selectItem(final int index) {
		addFilterElement(new Function<Node, Node>() {
			public Node apply(Node n) {
				path += String.format("[%d]", index);
				if (n instanceof ArgumentListNode) {
					return ((ArgumentListNode) n).items.get(index);
				}
				if (n instanceof ListNode) {
					return ((ListNode) n).items.get(index);
				}
				if (n instanceof StatementListNode) {
					return ((StatementListNode) n).items.get(index);
				}
				return null;
			}
		});
		return this;
	}

	public ASTPathFilter expectExpressionStatement(String attrName) {
		return this.selectPath(attrName).expect(ExpressionStatementNode.class);
	}

	public ASTPathFilter expectList(String attrName) {
		return this.selectPath(attrName).expect(ListNode.class);
	}

	public ASTPathFilter expectMemberExpression(String attrName) {
		return this.selectPath(attrName).expect(MemberExpressionNode.class);
	}

	public ASTPathFilter expectSetExpression(String attrName) {
		return this.selectPath(attrName).expect(SetExpressionNode.class);
	}

	public ASTPathFilter expectGetExpression(String attrName) {
		return this.selectPath(attrName).expect(GetExpressionNode.class);
	}
}
