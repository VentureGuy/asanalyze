package me.venturega.asanalyze.utils;

@FunctionalInterface
public interface Consumer<A, B> {
	public void accept(A a, B b);
}