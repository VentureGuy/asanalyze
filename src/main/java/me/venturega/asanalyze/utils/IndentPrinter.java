package me.venturega.asanalyze.utils;

import java.io.PrintWriter;

public class IndentPrinter {
	protected PrintWriter w;
	public int indent = 0;
	protected String indentChars = "\t";
	private String line="";

	public IndentPrinter(PrintWriter writer) {
		this.w = writer;
	}
	
	public PrintWriter getWriter() {
		return w;
	}
	
	protected String getIndent() {
		String o ="";
		for(int i=0;i<indent;i++)
			o+=indentChars;
		return o;
	}
	
	public IndentPrinter beginLine() {
		checkLine();
		line = "";
		return this;
	}
	
	// Used in unit tests.
	public IndentPrinter flush() {
		w.print(getIndent()+line);
		w.flush();
		return this;
	}
	
	private void checkLine() {
		if(line!="")
			println("");
	}

	public IndentPrinter indent() {
		checkLine();
		indent ++;
		return this;
	}
	
	public IndentPrinter dedent() {
		checkLine();
		indent = Math.max(0, indent-1);
		return this;
	}
	
	public IndentPrinter print(String data) {
		line+=data;
		return this;
	}
	
	public IndentPrinter println(String data) {
		w.println(getIndent()+line+data);
		line="";
		return this;
	}

	public String getLine() {
		return line;
	}
}
