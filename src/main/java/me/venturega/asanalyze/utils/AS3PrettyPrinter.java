/*
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package me.venturega.asanalyze.utils;

import static macromedia.asc.parser.Tokens.AS_TOKEN;
import static macromedia.asc.parser.Tokens.GET_TOKEN;
import static macromedia.asc.parser.Tokens.SET_TOKEN;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import flash.swf.tools.as3.EvaluatorAdapter;
import macromedia.asc.embedding.avmplus.RuntimeConstants;
import macromedia.asc.parser.ApplyTypeExprNode;
import macromedia.asc.parser.ArgumentListNode;
import macromedia.asc.parser.AttributeListNode;
import macromedia.asc.parser.BinaryExpressionNode;
import macromedia.asc.parser.BreakStatementNode;
import macromedia.asc.parser.CallExpressionNode;
import macromedia.asc.parser.CaseLabelNode;
import macromedia.asc.parser.CatchClauseNode;
import macromedia.asc.parser.ClassDefinitionNode;
import macromedia.asc.parser.ConditionalExpressionNode;
import macromedia.asc.parser.ContinueStatementNode;
import macromedia.asc.parser.DeleteExpressionNode;
import macromedia.asc.parser.DoStatementNode;
import macromedia.asc.parser.ExpressionStatementNode;
import macromedia.asc.parser.FinallyClauseNode;
import macromedia.asc.parser.ForStatementNode;
import macromedia.asc.parser.FunctionCommonNode;
import macromedia.asc.parser.FunctionDefinitionNode;
import macromedia.asc.parser.FunctionSignatureNode;
import macromedia.asc.parser.GetExpressionNode;
import macromedia.asc.parser.IdentifierNode;
import macromedia.asc.parser.IfStatementNode;
import macromedia.asc.parser.ImportDirectiveNode;
import macromedia.asc.parser.IncludeDirectiveNode;
import macromedia.asc.parser.IncrementNode;
import macromedia.asc.parser.InterfaceDefinitionNode;
import macromedia.asc.parser.ListNode;
import macromedia.asc.parser.LiteralArrayNode;
import macromedia.asc.parser.LiteralBooleanNode;
import macromedia.asc.parser.LiteralFieldNode;
import macromedia.asc.parser.LiteralNullNode;
import macromedia.asc.parser.LiteralNumberNode;
import macromedia.asc.parser.LiteralObjectNode;
import macromedia.asc.parser.LiteralRegExpNode;
import macromedia.asc.parser.LiteralStringNode;
import macromedia.asc.parser.LiteralVectorNode;
import macromedia.asc.parser.LiteralXMLNode;
import macromedia.asc.parser.MemberExpressionNode;
import macromedia.asc.parser.MetaDataEvaluator;
import macromedia.asc.parser.MetaDataNode;
import macromedia.asc.parser.Node;
import macromedia.asc.parser.PackageDefinitionNode;
import macromedia.asc.parser.PackageIdentifiersNode;
import macromedia.asc.parser.PackageNameNode;
import macromedia.asc.parser.ParameterListNode;
import macromedia.asc.parser.ParameterNode;
import macromedia.asc.parser.ProgramNode;
import macromedia.asc.parser.QualifiedIdentifierNode;
import macromedia.asc.parser.RestParameterNode;
import macromedia.asc.parser.ReturnStatementNode;
import macromedia.asc.parser.SelectorNode;
import macromedia.asc.parser.SetExpressionNode;
import macromedia.asc.parser.StatementListNode;
import macromedia.asc.parser.SuperExpressionNode;
import macromedia.asc.parser.SuperStatementNode;
import macromedia.asc.parser.SwitchStatementNode;
import macromedia.asc.parser.ThisExpressionNode;
import macromedia.asc.parser.ThrowStatementNode;
import macromedia.asc.parser.Tokens;
import macromedia.asc.parser.TryStatementNode;
import macromedia.asc.parser.TypeExpressionNode;
import macromedia.asc.parser.TypedIdentifierNode;
import macromedia.asc.parser.UnaryExpressionNode;
import macromedia.asc.parser.VariableBindingNode;
import macromedia.asc.parser.VariableDefinitionNode;
import macromedia.asc.parser.WhileStatementNode;
import macromedia.asc.parser.WithStatementNode;
import macromedia.asc.semantics.Value;
import macromedia.asc.util.Context;
import macromedia.asc.util.ObjectList;
import me.venturega.asanalyze.evaluators.SearchEvaluator;
import me.venturega.asanalyze.evaluators.SearchEvaluator.DoNotRecurseException;

/**
 * A partial pretty printer for generating AS3 from ASC AST's. It's used by data
 * binding when generating watchers for Array elements and function return
 * values. We use a PrettyPrinter to recreate the AS3 for the watcher args.
 */
public class AS3PrettyPrinter extends EvaluatorAdapter {
	public boolean TRACE = false;

	public AS3PrettyPrinter(PrintWriter out) {
		this(out, 0);
	}

	public AS3PrettyPrinter(PrintWriter out, int indent) {
		this.out = new IndentPrinter(out);
		this.indent = indent;
	}

	protected IndentPrinter out;
	protected int indent;
	protected HashMap<SelectorNode, Node> selectorBases = new HashMap<SelectorNode, Node>();

	private boolean suppress_semicolon = false;
	private boolean in_interface = false;

	protected void debug(Context cx, Node node, String nodeType) {
		if (TRACE) {
			TRACE = false;
			/*
			 * IndentPrinter oldIW = out; int oldIndent=indent; StringWriter sw
			 * = new StringWriter(); out = new IndentPrinter(new
			 * PrintWriter(sw)); node.evaluate(cx, this);
			 * System.out.println(nodeType+": "+sw.toString()); out=oldIW;
			 * indent=oldIndent;
			 */
			System.out.println(nodeType);
			TRACE = true;
		}
	}

	protected void expressionPrefixCheck(Context cx, Node node) {

		// Prefix, data gathering.
		CharacterizerVisitor cv = new CharacterizerVisitor();
		FindRecursive(cx, node, cv);
		if (cv.expressionPrefix != "")
			out.print(cv.expressionPrefix);
	}

	public class CharacterizerVisitor implements SearchEvaluator.Locator {

		public boolean parens;
		public boolean found_new;
		public String statementPrefix;
		public String statementSuffix;
		public String expressionPrefix;

		public CharacterizerVisitor() {
			parens = false;
			found_new = false;
			statementPrefix = "";
			statementSuffix = "";
			expressionPrefix = "";
		}
		
		public void setParens(boolean value) {
			parens = value;
		}
		public boolean getParens() {
			return parens;
		}
		
		public void setFoundNew(boolean value) {
			found_new = value;
		}
		public boolean getFoundNew() {
			return found_new;
		}
		
		public void setStatementPrefix(String value) {
			statementPrefix = value;
		}
		public String getStatementPrefix() {
			return statementPrefix;
		}
		
		public void setStatementSuffix(String value) {
			statementSuffix = value;
		}
		public String getStatementSuffix() {
			return statementSuffix;
		}
		
		public void setExpressionPrefix(String value) {
			expressionPrefix = value;
		}
		public String getExpressionPrefix() {
			return expressionPrefix;
		}

		@Override
		public Node apply(SearchEvaluator search, Node n) throws DoNotRecurseException {
			//String suffix = String.format(" P=%s, FN=%s, AP=\"%s\", AS=\"%s\", XP=\"%s\"", parens, found_new, statementPrefix, statementSuffix, expressionPrefix);
			if (n instanceof CallExpressionNode) {
				//System.out.println("CXN"+suffix);
				CallExpressionNode cxn = (CallExpressionNode) n;
				parens = !(cxn.expr instanceof IdentifierNode);
				found_new = cxn.is_new;
				search.stopRecursion();
			} else if (n instanceof DeleteExpressionNode) {
				//System.out.println("DXN"+suffix);
				expressionPrefix = "delete "; // This is so goddamn hacky.
				search.stopRecursion();
			} else if (n instanceof IncrementNode) {
				//System.out.println("INCN"+suffix);
				IncrementNode inc = (IncrementNode) n;
				if (inc.expr != null) {
					if (!inc.isPostfix) {
						statementPrefix = opStr(inc.op, false);
					}
					if (inc.isPostfix) {
						statementSuffix = opStr(inc.op, false);
					}
				}
				search.stopRecursion();
			}
			return null;
		}

	}

	protected void setExprPrefixCheck(Context cx, Node node) {
		if (TRACE) {
			System.out.println("setExprPrefixCheck(" + node.getClass().getCanonicalName() + ")");
		}

		// Prefix, data gathering.
		CharacterizerVisitor cv = new CharacterizerVisitor();
		FindRecursive(cx, node, cv);

		if (cv.found_new)
			out.print("new ");
	}

	public ArrayList<Node> FindRecursive(Context cx, Node N, SearchEvaluator.Locator test) {
		SearchEvaluator searcher = new SearchEvaluator(test);
		N.evaluate(cx, searcher);
		return searcher.found;
	}

	protected void wrapStatement(Context cx, Node node) {
		if (TRACE) {
			System.out.println("wrapStatement(" + node.getClass().getCanonicalName() + ")");
		}

		// Prefix, data gathering.
		CharacterizerVisitor cv = new CharacterizerVisitor();
		FindRecursive(cx, node, cv);
		
		if (cv.found_new) {
			out.print("new ");
			if (cv.parens)
				out.print("(");
		} else
			out.print(cv.statementPrefix);
		if (cv.expressionPrefix != "")
			out.print(cv.expressionPrefix);

		node.evaluate(cx, this);

		out.print(cv.statementSuffix);
		if (cv.parens)
			out.print(")");
	}

	private void wrapSelectorNode(Context cx, SelectorNode node) {
		out.print(getSelectorNodePrefix(node));
		wrapStatement(cx, node.expr);
		out.print(getSelectorNodeSuffix(node));
	}

	protected String getSelectorNodePrefix(SelectorNode node) {
		if (selectorBases.get(node) == null)
			return "";
		switch (node.getMode()) {
		case Tokens.LEFTBRACKET_TOKEN:
			return "[";
		case Tokens.EMPTY_TOKEN:
			return "/* mode=EMPTY */"; // ?!
		default:
			return ".";
		}
	}

	protected String getSelectorNodeSuffix(SelectorNode node) {
		if (selectorBases.get(node) == null)
			return "";
		switch (node.getMode()) {
		case Tokens.LEFTBRACKET_TOKEN:
			return "]";
		case Tokens.EMPTY_TOKEN:
			return "";
		default:
			return "";
		}
	}

	@Override
	public Value evaluate(Context cx, AttributeListNode node) {
		this.debug(cx, node, "AttributeListNode");
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			n.evaluate(cx, this);
			out.print(" ");
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ArgumentListNode node) {
		this.debug(cx, node, "ArgumentListNode");
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			if (n == null) {
				continue;
			}

			boolean parens = needsParens(n, true);
			if (parens)
				out.print("(");
			wrapStatement(cx, n);
			if (parens)
				out.print(")");

			if ((i + 1) < size) {
				out.print(", ");
			}
		}
		// hit_new = false;
		return null;
	}

	@Override
	public Value evaluate(Context cx, BinaryExpressionNode node) {
		this.debug(cx, node, "BinaryExpressionNode");
		boolean wrapInParens = false;
		switch (node.op) {
		case AS_TOKEN:
			wrapInParens = true;
			break;
		}
		if (wrapInParens)
			out.print("(");
		boolean p;
		if (node.lhs != null) {
			p = needsParens(node.lhs, true);
			if (p)
				out.print("(");

			// node.lhs.evaluate(cx, this);
			wrapStatement(cx, node.lhs);
			if (p)
				out.print(")");
		}

		op(node.op);

		if (node.rhs != null) {
			p = needsParens(node.rhs, true);
			if (p)
				out.print("(");
			// node.rhs.evaluate(cx, this);
			wrapStatement(cx, node.rhs);
			if (p)
				out.print(")");
		}
		if (wrapInParens)
			out.print(")");
		return null;
	}

	private boolean needsParens(Node node, boolean strictLists) {
		if (!strictLists && node instanceof ListNode) {
			return needsParens(((ListNode) node).items.at(0), true);
		}
		if (node instanceof ConditionalExpressionNode)
			return true;
		if (node instanceof BinaryExpressionNode)
			return true;
		if (node instanceof ListNode)
			return true;
		return false;
	}

	@Override
	public Value evaluate(Context cx, CallExpressionNode node) {
		this.debug(cx, node, "CallExpressionNode");
		if (node.expr != null) {
			wrapSelectorNode(cx, node);
		}
		out.print("(");
		if (node.args != null) {
			node.args.evaluate(cx, this);
		}
		out.print(")");
		return null;
	}

	/*
	 * @Override public Value evaluate(Context cx, PackageDefinitionNode node) {
	 * this.debug(cx, node, "PackageDefinitionNode"); return null; }
	 */

	@Override
	public Value evaluate(Context cx, ClassDefinitionNode node) {
		this.debug(cx, node, "ClassDefinitionNode");
		out.beginLine();

		if (node.attrs != null) {
			node.attrs.evaluate(cx, this);
		}

		out.print("class ");

		String name = "";
		if ((node.name != null) && (node.name.name != null)) {
			name = node.name.name;
		} else if ((node.cframe != null) && (node.cframe.builder != null)) {
			name = node.cframe.builder.classname.toString();
		} else {
			assert false : "No class name found for ClassDefinitionNode";
		}

		out.print(name);

		if (node.baseclass != null) {
			out.print(" extends ");
			node.baseclass.evaluate(cx, this);
		}
		if (node.interfaces != null) {
			out.beginLine();
			out.print(" implements ");
			node.interfaces.evaluate(cx, this);
		}
		out.beginLine().print("{").indent();
		{
			if (node.statements != null) {
				ObjectList<Node> initializer = new ObjectList<Node>();
				ObjectList<Node> statements = new ObjectList<Node>();
				for (int i = 0, size = node.statements.items.size(); i < size; i++) {
					Node sn = node.statements.items.get(i);
					if (sn == null)
						continue;
					if (sn instanceof ExpressionStatementNode) {
						initializer.add(sn);
					} else {
						statements.add(sn);
					}
				}
				boolean wrote_static_init = false;
				if (!statements.isEmpty()) {
					for (int i = 0, size = statements.size(); i < size; i++) {
						Node sn = statements.get(i);
						if (sn == null)
							continue;
						// Inject static initializer right before the first
						// FunctionDefinitionNode.
						if (!wrote_static_init && (sn instanceof FunctionDefinitionNode)) {
							if (!initializer.isEmpty()) {
								out.beginLine().print("{ // static initializer").indent();
								for (int j = 0, jsize = initializer.size(); j < jsize; j++) {
									Node ssn = initializer.get(j);
									if (ssn == null)
										continue;
									// out.print("/* "+ssn.toString()+" */");
									ssn.evaluate(cx, this);
								}
								out.dedent().beginLine().print("} // static initializer");
							}
							wrote_static_init = true;
						}
						// out.print("/* "+sn.toString()+" */");
						sn.evaluate(cx, this);
					}
				}
				// node.statements.evaluate(cx, this);
			}
		}
		out.dedent().beginLine().print("} // class " + name);
		return null;
	}

	public Value evaluate(Context cx, ForStatementNode node) {
		this.debug(cx, node, "ForStatementNode");
		out.beginLine();
		out.print("for");
		// BUG: NOT IMPLEMENTED IN FLEX FOR SOME FUCKING REASON.
		// I PATCHED FLEX. WILL FAIL IN STANDARD BUILDS.
		if (node.is_each) {
			out.print(" each");
		}
		out.print(" (");
		suppress_semicolon = true;
		if (node.is_forin) {
			if (node.initialize != null) {
				node.initialize.evaluate(cx, this);
			}
			out.print(" in ");
			if (node.test != null) {
				node.test.evaluate(cx, this);
			}
		} else {
			if (node.initialize != null) {
				node.initialize.evaluate(cx, this);
			}
			out.print("; ");
			if (node.test != null) {
				node.test.evaluate(cx, this);
			}
			out.print("; ");
			if (node.increment != null) {
				node.increment.evaluate(cx, this);
			}
		}
		suppress_semicolon = false;
		out.print(")");
		out.beginLine().print("{").indent();
		if (node.statement != null) {
			node.statement.evaluate(cx, this);
		}
		out.dedent().beginLine().print("}");

		return null;
	}

	public Value evaluate(Context cx, FunctionCommonNode node) {
		this.debug(cx, node, "FunctionCommonNode");
		boolean isAnonymous = false;
		boolean isVoid = false;
		if (node.identifier != null && node.identifier.name == "") {
			out.print("function"); // Anonymous method
			isAnonymous = true;
		}
		out.print("(");
		if (node.signature != null && node.signature.parameter != null) {
			node.signature.parameter.evaluate(cx, this);
		}
		out.print("):");
		if (node.signature != null && node.signature.result != null) {
			node.signature.result.evaluate(cx, this);
		} else {
			isVoid = true;
			out.print("void");
		}
		if (in_interface) {
			out.print(";");
			return null;
		}
		String signature = out.getLine();
		out.beginLine();
		out.print("{");
		out.indent().beginLine();
		if (node.body != null) {
			// Anonymous methods have sensible ReturnStatements.
			if (!isAnonymous || isVoid)
				node.body.items.removeLast(); // BUG: ALWAYS a return 0; at the
												// end, for some fucking reason.
			node.body.evaluate(cx, this);
		}
		out.dedent().beginLine();
		out.print("} // " + signature.strip());
		out.beginLine();
		return null;
	}

	public Value evaluate(Context cx, FunctionDefinitionNode node) {
		this.debug(cx, node, "FunctionDefinitionNode");
		out.beginLine();
		if (node.attrs != null) {
			node.attrs.evaluate(cx, this);
		}
		out.print("function ");
		if (node.name != null) {
			switch (node.name.kind) {
			case GET_TOKEN:
				out.print("get ");
				break;
			case SET_TOKEN:
				out.print("set ");
				break;
			}
			out.print(node.name.identifier.name);
		}
		if (node.fexpr != null) {
			node.fexpr.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, GetExpressionNode node) {
		this.debug(cx, node, "GetExpressionNode");
		if (node.expr != null) {
			wrapSelectorNode(cx, node);
		}
		return null;
	}

	public Value evaluate(Context cx, IdentifierNode node) {
		this.debug(cx, node, "IdentifierNode");
		out.print(node.name);
		return null;
	}

	public Value evaluate(Context cx, IfStatementNode node) {
		this.debug(cx, node, "IfStatementNode");
		out.beginLine();
		out.print("if (");
		if (node.condition != null) {
			node.condition.evaluate(cx, this);
		}
		out.print(")");
		out.beginLine();
		out.print("{");
		out.indent().beginLine();
		if (node.thenactions != null) {
			node.thenactions.evaluate(cx, this);
		}
		out.dedent().beginLine();
		out.print("}");
		if (node.elseactions != null) {
			out.print(" else {");
			out.indent().beginLine();
			node.elseactions.evaluate(cx, this);
			out.dedent().beginLine();
			out.print("}");
		}
		return null;
	}

	public Value evaluate(Context cx, InterfaceDefinitionNode node) {
		this.debug(cx, node, "InterfaceDefinitionNode");
		out.beginLine();

		if (node.attrs != null) {
			node.attrs.evaluate(cx, this);
		}

		out.print("interface ");

		String name = "";
		if ((node.name != null) && (node.name.name != null)) {
			name = node.name.name;
		} else if ((node.cframe != null) && (node.cframe.builder != null)) {
			name = node.cframe.builder.classname.toString();
		} else {
			assert false : "No class name found for ClassDefinitionNode";
		}

		out.print(name);

		/*
		 * I think this is incorrect from the spec; Adobe docs says that each
		 * interface can extend *MULTIPLE* interfaces, and nothing about
		 * implementing. Whatever.
		 */
		if (node.baseclass != null) {
			out.print(" extends ");
			node.baseclass.evaluate(cx, this);
		}
		if (node.interfaces != null) {
			out.beginLine();
			out.print(" implements ");
			node.interfaces.evaluate(cx, this);
		}
		out.beginLine().print("{").indent();
		{
			in_interface = true;
			if (node.statements != null) {
				node.statements.evaluate(cx, this);
			}
			in_interface = false;
		}
		out.dedent().beginLine().print("} // interface " + name);
		return null;
	}

	public Value evaluate(Context cx, ListNode node) {
		this.debug(cx, node, "ListNode");
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			wrapStatement(cx, n);
			// n.evaluate(cx, this);
			if ((i + 1) < size) {
				out.print(", ");
			}
		}
		return null;
	}

	public Value evaluate(Context cx, LiteralArrayNode node) {
		this.debug(cx, node, "LiteralArrayNode");
		out.print("[");
		if (node.elementlist != null)
			evaluate(cx, node.elementlist);
		out.print("]");
		return null;
	}

	public Value evaluate(Context cx, LiteralVectorNode node) {
		this.debug(cx, node, "LiteralVectorNode");
		out.print("new<");
		node.type.evaluate(cx, this);
		out.print(">");
		out.print("[");
		evaluate(cx, node.elementlist);
		out.print("]");
		return null;
	}

	public Value evaluate(Context cx, LiteralBooleanNode node) {
		this.debug(cx, node, "LiteralBooleanNode");
		out.print(node.value ? "true" : "false");
		return null;
	}

	public Value evaluate(Context cx, LiteralFieldNode node) {
		this.debug(cx, node, "LiteralFieldNode");
		if (node.name != null) {
			node.name.evaluate(cx, this);
		}

		out.print(": ");

		if (node.value != null) {
			node.value.evaluate(cx, this);
		}

		return null;
	}

	public Value evaluate(Context cx, LiteralNumberNode node) {
		this.debug(cx, node, "LiteralNumberNode");
		out.print(node.value);
		return null;
	}

	public Value evaluate(Context cx, LiteralObjectNode node) {
		this.debug(cx, node, "LiteralObjectNode");
		out.print("{");
		if (node.fieldlist != null)
			node.fieldlist.evaluate(cx, this);
		out.print("}");
		return null;
	}

	public Value evaluate(Context cx, LiteralStringNode node) {
		this.debug(cx, node, "LiteralStringNode");
		if (node.isDoubleQuote()) {
			out.print("\"" + node.value.replace("\\", "\\\\").replace("\t", "\\t").replace("\n", "\\n")
					.replace("\r", "\\r").replace("\"", "\\\"") + "\"");
		} else if (node.isSingleQuote()) {
			out.print("'" + node.value.replace("\\", "\\\\").replace("\t", "\\t").replace("\n", "\\n")
					.replace("\r", "\\r").replace("\'", "\\\'") + "\'");
		} else {
			out.print(node.value); // In LiteralXMLNodes.
		}
		return null;
	}

	public Value evaluate(Context cx, LiteralNullNode node) {
		this.debug(cx, node, "LiteralNullNode");
		out.print("null");
		return null;
	}

	public Value evaluate(Context cx, LiteralRegExpNode node) {
		this.debug(cx, node, "LiteralRegExpNode");
		out.print(node.value);
		return null;
	}

	@Override
	public Value evaluate(Context cx, MemberExpressionNode node) {
		this.debug(cx, node, "MemberExpressionNode");
		if (node.base != null) {
			/*
			Node subj = node.base;
			if (subj instanceof ListNode)
				subj = ((ListNode) node.base).first();
			else if (subj instanceof ArgumentListNode)
				subj = ((ListNode) node.base).first();
			if (subj instanceof MemberExpressionNode) {
				MemberExpressionNode msubj = ((MemberExpressionNode) node.base);
				if (msubj.base == null && msubj.selector != null)
					subj = msubj.selector;
			}
			boolean parens = needsParens(subj, false);
			if (!parens && subj instanceof CallExpressionNode) {
				parens = ((CallExpressionNode) subj).is_new;
			}
			if (!parens && subj instanceof SelectorNode) {
				parens = ((SelectorNode) subj).isRValue(); // ?
			}
			if (parens) {
				out.print("(");
			}
			*/
			wrapStatement(cx, node.base);
			/*
			if (parens) {
				out.print(")");
			}
			*/
		}

		if (node.selector != null) {
			selectorBases.put(node.selector, node.base);
			wrapSelectorNode(cx, node.selector);
		}
		if (node.base == null && node.selector == null) {
			out.print("/* MXN has neither a base nor a selector.*/");
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, SetExpressionNode node) {
		this.debug(cx, node, "SetExpressionNode");
		// out.beginLine();
		if (node.expr != null) {
			wrapSelectorNode(cx, node);
		}
		op(node.getMode(), true, " = ");
		if (node.args != null) {
			// boolean old_hit_new = hit_new;
			// hit_new = false;
			// setExprPrefixCheck(cx, node.args);
			node.args.evaluate(cx, this);
			// hit_new = old_hit_new;
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ProgramNode node) {
		this.debug(cx, node, "ProgramNode");

		if (node.statements != null) {
			node.statements.evaluate(cx, this);
		}
		out.beginLine();
		return null;
	}

	public Value evaluate(Context cx, QualifiedIdentifierNode node) {
		this.debug(cx, node, "QualifiedIdentifierNode");
		if (node.qualifier != null) {
			if (node.qualifier instanceof MemberExpressionNode) {
				node.qualifier.evaluate(cx, this);
				out.print("::");
			}
			if (node.qualifier instanceof LiteralStringNode) {
				node.qualifier.evaluate(cx, this);
				out.print(".");
			}
		}
		evaluate(cx, (IdentifierNode) node);
		return null;
	}

	public Value evaluate(Context cx, ReturnStatementNode node) {
		this.debug(cx, node, "ReturnStatementNode");
		out.beginLine();
		out.print("return");
		if (node.expr != null) {
			out.print(" ");
			// boolean old_hit_new = hit_new;
			// hit_new = false;
			setExprPrefixCheck(cx, node.expr);
			node.expr.evaluate(cx, this);
			// hit_new = old_hit_new;
		}
		out.print(";");
		return null;
	}

	public Value evaluate(Context cx, ThisExpressionNode node) {
		this.debug(cx, node, "ThisExpressionNode");
		out.print("this");
		return null;
	}

	@Override
	public Value evaluate(Context cx, TypedIdentifierNode node) {
		this.debug(cx, node, "TypedIdentifierNode");
		if (node.identifier != null) {
			node.identifier.evaluate(cx, this);
		}
		if (node.type != null) {
			out.print(":");
			node.type.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, VariableDefinitionNode node) {
		this.debug(cx, node, "VariableDefinitionNode");
		if (!suppress_semicolon) {
			out.beginLine();
		}
		if (node.attrs != null) {
			node.attrs.evaluate(cx, this);
		}
		switch (node.kind) {
		case Tokens.VAR_TOKEN:
			out.print("var ");
			break;
		case Tokens.CONST_TOKEN:
			out.print("const ");
			break;
		default:
			out.print("/* kind=" + Integer.toString(node.kind) + " */ ");
			break;
		}
		if (node.list != null) {
			node.list.evaluate(cx, this);
		}
		if (!suppress_semicolon) {
			out.print(";");
		}
		return null;
	}

	public Value evaluate(Context cx, VariableBindingNode node) {
		this.debug(cx, node, "VariableBindingNode");
		if (node.variable != null)
			node.variable.evaluate(cx, this);
		if (node.initializer != null) {
			out.print(" = ");
			setExprPrefixCheck(cx, node.initializer);
			expressionPrefixCheck(cx, node.initializer);
			node.initializer.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context unused_cx, ImportDirectiveNode node) {
		Context cx = node.cx;
		out.print("import ");
		if (node.attrs != null) {
			node.attrs.evaluate(cx, this);
		}
		if (node.name != null) {
			node.name.evaluate(cx, this);
		}
		out.print(";");
		return null;
	}

	public Value evaluate(Context cx, StatementListNode node) {
		this.debug(cx, node, "StatementListNode");
		if (node.config_attrs != null) {
			out.beginLine();
			for (int i = 0, size = node.config_attrs.items.size(); i < size; i++) {
				node.config_attrs.items.get(i).evaluate(cx, this);
			}
			out.beginLine().print("{").indent();
		}
		PackageDefinitionNode current_package = null;
		String current_package_name = null;
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			if (n != null) {
				boolean comment = false;
				if (n instanceof PackageDefinitionNode) {
					// OK, the way this appears to work is that there is a
					// PDN at the start of each package block, and a PDN at the
					// end.
					// They APPEAR to be identical.
					if (current_package == null) {
						out.beginLine();
						current_package = (PackageDefinitionNode) n;
						current_package.evaluate(cx, this);
						current_package_name = current_package.name.id.toIdentifierString();
						out.beginLine().print("{");
						out.indent().beginLine();
					} else {
						out.dedent().beginLine().print("} // package " + current_package_name);
						current_package_name = null;
						current_package = null;
					}
					continue;
				}
				if (n instanceof ImportDirectiveNode) {
					ImportDirectiveNode importD = (ImportDirectiveNode) n;
					// import __AS3__.vec.Vector; <- Internal shit, I think.
					if (importD.name.id.list.at(0).name == "__AS3__")
						continue; // comment = true;
				}
				out.beginLine();
				if (comment)
					out.print("/* ");
				n.evaluate(cx, this);
				if (comment)
					out.print(" */");
			}
		}
		if (node.config_attrs != null) {
			out.dedent().beginLine().print("}");
		}
		return null;
	}

	public Value evaluate(Context cx, PackageNameNode node) {
		this.debug(cx, node, "PackageNameNode");

		for (int i = 0; i < node.id.list.size(); i++) {
			if (i > 0) {
				out.print(".");
			}
			out.print(node.id.list.get(i).name);
		}
		return null;
	}

	public Value evaluate(Context cx, ConditionalExpressionNode node) {
		this.debug(cx, node, "ConditionalExpressionNode");
		if (node.condition != null) {
			node.condition.evaluate(cx, this);
		}
		out.print(" ? ");
		if (node.thenexpr != null) {
			setExprPrefixCheck(cx, node.thenexpr);
			node.thenexpr.evaluate(cx, this);
		}
		out.print(" : ");
		if (node.elseexpr != null) {
			setExprPrefixCheck(cx, node.elseexpr);
			node.elseexpr.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, UnaryExpressionNode node) {
		this.debug(cx, node, "UnaryExpressionNode");
		op(node.op, false);
		if (node.expr != null) {
			boolean parens = needsParens(node.expr, false);
			if (parens)
				out.print("(");
			wrapStatement(cx, node.expr);
			// node.expr.evaluate(cx, this);
			if (parens)
				out.print(")");
		}
		return null;
	}

	public Value evaluate(Context cx, WhileStatementNode node) {
		this.debug(cx, node, "WhileStatementNode");
		out.beginLine();
		out.print("while (");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.print(")");
		out.beginLine().print("{").indent();
		if (node.statement != null) {
			node.statement.evaluate(cx, this);
		}
		out.dedent().beginLine().print("}");
		return null;
	}

	public Value evaluate(Context cx, WithStatementNode node) {
		this.debug(cx, node, "WithStatementNode");
		out.beginLine();
		out.print("with (");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.print(")");
		out.beginLine().print("{").indent();
		if (node.statement != null) {
			node.statement.evaluate(cx, this);
		}
		out.dedent().beginLine().print("}");
		return null;
	}

	public Value evaluate(Context cx, PackageDefinitionNode node) {
		this.debug(cx, node, "PackageDefinitionNode");
		out.print(String.format("package %s", (node.name.id.toIdentifierString())));
		return null;
	}

	public Value evaluate(Context cx, PackageIdentifiersNode node) {
		this.debug(cx, node, "PackageIdentifiersNode");
		// for (IdentifierNode n : node.list)
		for (int i = 0, size = node.list.size(); i < size; i++) {
			IdentifierNode n = node.list.get(i);
			n.evaluate(cx, this);
		}
		return null;
	}

	private void op(int op) {
		op(op, true);
	}

	private void op(int op, boolean spaces) {
		op(op, spaces, null);
	}

	private void op(int op, boolean spaces, String defaultStr) {
		out.print(opStr(op, spaces, defaultStr));
	}
	
	private String opStr(int op) {
		return opStr(op, true);
	}
	
	private String opStr(int op, boolean spaces) {
		return opStr(op, spaces, null);
	}

	private String opStr(int op, boolean spaces, String defaultStr) {
		switch (op) {
		case Tokens.ASSIGN_TOKEN: {
			return spaces ? " = " : "=";
		}
		//case Tokens.DOT_TOKEN: {
		//	return ".";
		//}
		case Tokens.NOT_TOKEN: {
			return "!";
		}
		case Tokens.BITWISENOT_TOKEN: {
			return "~";
		}
		case Tokens.NOTEQUALS_TOKEN: {
			return " != ";
		}
		case Tokens.STRICTNOTEQUALS_TOKEN: {
			return " !== ";
		}
		case Tokens.LESSTHAN_TOKEN: {
			return " < ";
		}
		case Tokens.LESSTHANOREQUALS_TOKEN: {
			return " <= ";
		}
		case Tokens.EQUALS_TOKEN: {
			return " == ";
		}
		case Tokens.STRICTEQUALS_TOKEN: {
			return " === ";
		}
		case Tokens.GREATERTHAN_TOKEN: {
			return " > ";
		}
		case Tokens.GREATERTHANOREQUALS_TOKEN: {
			return " >= ";
		}
		case Tokens.MULTASSIGN_TOKEN: {
			return " *= ";
		}
		case Tokens.MULT_TOKEN: {
			return " * ";
		}
		case Tokens.DIVASSIGN_TOKEN: {
			return " /= ";
		}
		case Tokens.DIV_TOKEN: {
			return " / ";
		}
		case Tokens.MODULUSASSIGN_TOKEN: {
			return " %= ";
		}
		case Tokens.MODULUS_TOKEN: {
			return " % ";
		}
		case Tokens.PLUSASSIGN_TOKEN: {
			return " += ";
		}
		case Tokens.PLUS_TOKEN: {
			return " + ";
		}
		case Tokens.MINUSASSIGN_TOKEN: {
			return " -= ";
		}
		case Tokens.MINUS_TOKEN: {
			return " - ";
		}
		case Tokens.LEFTSHIFTASSIGN_TOKEN: {
			return " <<= ";
		}
		case Tokens.LEFTSHIFT_TOKEN: {
			return " << ";
		}
		case Tokens.RIGHTSHIFTASSIGN_TOKEN: {
			return " >>= ";
		}
		case Tokens.RIGHTSHIFT_TOKEN: {
			return " >> ";
		}
		case Tokens.UNSIGNEDRIGHTSHIFTASSIGN_TOKEN: {
			return " >>>= ";
		}
		case Tokens.UNSIGNEDRIGHTSHIFT_TOKEN: {
			return " >>> ";
		}
		case Tokens.BITWISEANDASSIGN_TOKEN: {
			return " &= ";
		}
		case Tokens.BITWISEAND_TOKEN: {
			return " & ";
		}
		case Tokens.BITWISEXORASSIGN_TOKEN: {
			return " |= ";
		}
		case Tokens.BITWISEXOR_TOKEN: {
			return " ^ ";
		}
		case Tokens.BITWISEORASSIGN_TOKEN: {
			return " ^= ";
		}
		case Tokens.BITWISEOR_TOKEN: {
			return " | ";
		}
		case Tokens.LOGICALANDASSIGN_TOKEN: {
			return " &&= ";
		}
		case Tokens.LOGICALAND_TOKEN: {
			return " && ";
		}
		case Tokens.LOGICALXORASSIGN_TOKEN: {
			return " ^^= ";
		}
		case Tokens.LOGICALXOR_TOKEN: {
			return " ^^ ";
		}
		case Tokens.LOGICALORASSIGN_TOKEN: {
			return " ||= ";
		}
		case Tokens.LOGICALOR_TOKEN: {
			return " || ";
		}
		// 5/2/2019, used in coersive BinaryExpressionNodes. - VG
		case Tokens.AS_TOKEN: {
			return " as ";
		}
		// 5/10/2019, used in BinaryExpressionNodes.
		case Tokens.IS_TOKEN: {
			return " is ";
		}
		// 5/10/2019, used in BinaryExpressionNodes
		case Tokens.IN_TOKEN: {
			return " in ";
		}
		// 5/11/2019, used in IncrementNodes
		case Tokens.PLUSPLUS_TOKEN: {
			return spaces ? " ++ " : "++";
		}
		case Tokens.MINUSMINUS_TOKEN: {
			return spaces ? " -- " : "--";
		}
		default: {
			if(defaultStr!=null)
				return defaultStr;
			String tcn = String.format("UNKNOWN TOKEN ID %d (tokenClassName index %d)", op, op*-1);
			if((op*-1)<Tokens.tokenClassNames.length)
				tcn = Tokens.tokenClassNames[-1*op];
			assert false : "Unhandled operation, " + op + " ("+tcn+").";
		}
		}
		return "";
	}

	@Override
	public Value evaluate(Context cx, FunctionSignatureNode node) {
		this.debug(cx, node, "FunctionSignatureNode");
		// Function parameter (parameter:result)
		if (node.result != null) {
			node.result.evaluate(cx, this);
		}

		if (node.parameter != null) {
			node.parameter.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParameterListNode node) {
		this.debug(cx, node, "ParameterListNode");
		for (int i = 0; i < node.items.size(); i++) {
			if (i > 0)
				out.print(", ");
			node.items.get(i).evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParameterNode node) {
		this.debug(cx, node, "ParameterNode");
		if (node.identifier != null) {
			node.identifier.evaluate(cx, this);
		}
		out.print(":");
		if (node.type != null) {
			node.type.evaluate(cx, this);
		} else {
			out.print("*");
		}
		if (node.init != null) {
			out.print(" = ");
			node.init.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ExpressionStatementNode node) {
		this.debug(cx, node, "ExpressionStatementNode");
		out.beginLine();
		wrapStatement(cx, node.expr);
		out.print(";");
		return null;
	}

	@Override
	public Value evaluate(Context cx, SwitchStatementNode node) {
		this.debug(cx, node, "SwitchStatementNode");
		out.beginLine();
		out.print("switch (");
		node.expr.evaluate(cx, this);
		out.print(") {");
		out.indent().indent(); // HAX: case/default will dedent once.
		if (node.statements != null) {
			node.statements.evaluate(cx, this);
		}
		out.dedent().dedent().beginLine();
		out.print("}");
		return null;
	}

	@Override
	public Value evaluate(Context cx, SuperStatementNode node) {
		this.debug(cx, node, "SuperStatementNode");
		// out.print("super");
		out.beginLine();
		if (node.call != null) {
			expressionPrefixCheck(cx, node.call);
			// out.print(".");
			node.call.evaluate(cx, this);
		}
		out.print(";");
		return null;
	}

	@Override
	public Value evaluate(Context cx, SuperExpressionNode node) {
		this.debug(cx, node, "SuperExpressionNode");
		out.print("super");
		if (node.expr != null) {
			out.print(".");
			node.expr.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, CaseLabelNode node) {
		this.debug(cx, node, "CaseLabelNode");
		out.dedent().beginLine();
		if (node.label != null) {
			out.print("case ");
			node.label.evaluate(cx, this);
			out.print(":");
		} else {
			out.print("default:");
		}
		out.indent();
		return null;
	}

	@Override
	public Value evaluate(Context cx, ContinueStatementNode node) {
		this.debug(cx, node, "ContinueStatementNode");
		out.beginLine();
		out.print("continue");
		if (node.id != null) {
			out.print(" ");
			node.id.evaluate(cx, this);
		}
		out.print(";");
		return null;
	}

	@Override
	public Value evaluate(Context cx, BreakStatementNode node) {
		this.debug(cx, node, "BreakStatementNode");
		out.beginLine();
		out.print("break");
		if (node.id != null) {
			out.print(" ");
			node.id.evaluate(cx, this);
		}
		out.print(";");
		return null;
	}

	@Override
	public Value evaluate(Context cx, TypeExpressionNode node) {
		this.debug(cx, node, "TypeExpressionNode");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, IncrementNode node) {
		this.debug(cx, node, "IncrementNode");
		if (node.expr != null) {
			if (node.expr != null) {
				wrapStatement(cx, node.expr);
			}
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, DeleteExpressionNode node) {
		this.debug(cx, node, "DeleteExpressionNode");
		// out.beginLine().print("delete ");
		if (node.expr != null) {
			wrapSelectorNode(cx, node);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ApplyTypeExprNode node) {
		this.debug(cx, node, "ApplyTypeExprNode");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.print(".<");
		if (node.typeArgs != null) {
			for (int i = 0, size = node.typeArgs.items.size(); i < size; i++) {
				Node child = node.typeArgs.items.get(i);
				if (child != null) {
					if (i > 0) {
						out.print(", ");
					}
					child.evaluate(cx, this);
				}
			}
		}
		out.print(">");
		return null;
	}

	@Override
	public Value evaluate(Context cx, RestParameterNode node) {
		this.debug(cx, node, "RestParameterNode");
		out.print("... ");
		if (node.parameter != null) {
			node.parameter.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ThrowStatementNode node) {
		this.debug(cx, node, "ThrowStatementNode");
		out.print("throw ");
		if (node.expr != null) {
			// boolean old_new = hit_new;
			// hit_new = false;
			setExprPrefixCheck(cx, node.expr);
			node.expr.evaluate(cx, this);
			// hit_new=old_new;
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, IncludeDirectiveNode node) {
		this.debug(cx, node, "IncludeDirectiveNode");
		out.beginLine();
		out.print("include ");
		if (node.original_filespec != null) {
			node.original_filespec.evaluate(cx, this);
		} else {
			if (node.filespec != null) {
				node.filespec.evaluate(cx, this);
			}
		}
		out.print(";");
		return null;
	}

	@Override
	public Value evaluate(Context cx, LiteralXMLNode node) {
		this.debug(cx, node, "LiteralXMLNode");
		if (node.list != null) {
			node.list.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, TryStatementNode node) {
		this.debug(cx, node, "TryStatementNode");
		out.beginLine().print("try");
		out.beginLine().print("{").indent();
		if (node.tryblock != null) {
			node.tryblock.evaluate(cx, this);
		}
		out.beginLine().dedent().print("}");
		if (node.catchlist != null) {
			for (int i = 0, size = node.catchlist.items.size(); i < size; i++) {
				Node n = node.catchlist.items.get(i);
				if (n != null)
					n.evaluate(cx, this);
			}
		}
		if (node.finallyblock != null) {
			node.finallyblock.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, CatchClauseNode node) {
		this.debug(cx, node, "CatchClauseNode");
		out.beginLine().print("catch(");
		if (node.parameter != null) {
			node.parameter.evaluate(cx, this);
		}
		out.print(")");
		out.beginLine().print("{").indent();
		if (node.statements != null) {
			node.statements.evaluate(cx, this);
		}
		out.beginLine().dedent().print("}");
		return null;
	}

	@Override
	public Value evaluate(Context cx, FinallyClauseNode node) {
		this.debug(cx, node, "FinallyClauseNode");
		out.beginLine().print("finally");
		out.beginLine().print("{").indent();
		if (node.statements != null) {
			node.statements.evaluate(cx, this);
		}
		out.beginLine().dedent().print("}");
		return null;
	}

	@Override
	public Value evaluate(Context cx, DoStatementNode node) {
		this.debug(cx, node, "DoStatementNode");
		out.beginLine().print("do");
		out.beginLine().print("{").indent();
		if (node.statements != null) {
			node.statements.evaluate(cx, this);
		}
		out.beginLine().dedent().print("} while(");
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		out.print(")");
		return null;
	}

	private void printValueString(Value v, String data) {
		switch (v.getTypeId()) {
		case RuntimeConstants.TYPE_array:
			out.print("[" + data + "]");
			break;
		case RuntimeConstants.TYPE_string:
			out.print("\"" + data + "\"");
			break;
		default:
			if (StringUtils.isNumeric(data) || data == "true" || data == "false")
				out.print(data);
			else
				out.print("\"" + data + "\"");
			break;
		}
	}

	@Override
	public Value evaluate(Context cx, MetaDataNode node) {
		this.debug(cx, node, "MetaDataNode");
		out.beginLine().print("[").print(node.getId());
		Value[] vals = node.getValues();
		if (vals != null && vals.length > 0) {
			out.print("(");
			for (int i = 0, size = vals.length; i < size; i++) {
				Value v = vals[i];
				if (i > 0) {
					out.print(", ");
				}

				if (v instanceof MetaDataEvaluator.KeylessValue) {
					printValueString(v, ((MetaDataEvaluator.KeylessValue) v).obj);
				}
				if (v instanceof MetaDataEvaluator.KeyValuePair) {
					out.print(((MetaDataEvaluator.KeyValuePair) v).key);
					out.print("=");
					printValueString(v, ((MetaDataEvaluator.KeyValuePair) v).obj);
				}
			}
			out.print(")");
		}
		out.print("]");
		return null;
	}

	public void flush() {
		out.flush();
	}
}
