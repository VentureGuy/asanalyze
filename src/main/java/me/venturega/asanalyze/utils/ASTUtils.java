package me.venturega.asanalyze.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.function.Function;

import macromedia.asc.parser.*;
import macromedia.asc.util.Context;
import macromedia.asc.util.ContextStatics;
import me.venturega.asanalyze.evaluators.ParentizerEvaluator;
import me.venturega.asanalyze.evaluators.SearchEvaluator;
import me.venturega.asanalyze.evaluators.SearchEvaluator.DoNotRecurseException;
import me.venturega.asanalyze.utils.AstAnalyzer;
public class ASTUtils {
	public static BinaryExpressionNode getIfRootBinaryEx(IfStatementNode ifs) {
		return ((BinaryExpressionNode)((ListNode)ifs.condition).items.get(0));
	}
	public static MemberExpressionNode getRootMemberExpressionNode(ParentizerEvaluator parents, MemberExpressionNode mxn) {
		Node current = (Node)mxn;
		Node parent = null; 
		while(true) {
			parent = parents.getParentOf(current);
			if (!(parent instanceof MemberExpressionNode)) {
				break;
			}
			current=parent;
		}
		return (MemberExpressionNode) current;
	}
	
	
	public static int GetIfOperator(IfStatementNode ifs) {
		return getIfRootBinaryEx(ifs).op;
	}

	/**
	 * Get Right Hand Side (RHS) of the if-statement.
	 * @param ifs
	 * @return
	 */
	public static Node GetIfRHS(IfStatementNode ifs) {
		return getIfRootBinaryEx(ifs).rhs;
	}

	/**
	 * Get Left Hand Side (LHS) of the if-statement
	 * @param ifs
	 * @return
	 */
	public static Node GetIfLHS(IfStatementNode ifs) {
		return getIfRootBinaryEx(ifs).lhs;
	}
	
	public static boolean IsAssignment(Node node) {
		// @formatter:off
		/*
          <ExpressionStatementNode>
            <ListNode>
              <MemberExpressionNode>
                <selector>
                  <SetExpressionNode is_constinit="false" is_initializer="false" is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
                    <IdentifierNode name="TX"/>
                    <ArgumentListNode>
                      <MemberExpressionNode>
                        <selector>
                          <CallExpressionNode is_new="true" is_package="false" isRValue="false" isAttr="false" isSuper="false" isThis="false" isVoidResult="false" mode="lexical">
                            <IdentifierNode name="TextData"/>
                          </CallExpressionNode>
                        </selector>
                      </MemberExpressionNode>
                    </ArgumentListNode>
                  </SetExpressionNode>
                </selector>
              </MemberExpressionNode>
            </ListNode>
          </ExpressionStatementNode>
          */
		// @formatter:on
		if(!(node instanceof ExpressionStatementNode)) return false;
		return Contains(SetExpressionNode.class, node);
	}
	private static boolean Contains(Class<?> cls, Node node) {
		if(node == null) return false;
		if(node.getClass().isAssignableFrom(cls))
			return true;
		if(node instanceof ExpressionStatementNode) {
			return Contains(cls, ((ExpressionStatementNode)node).expr);
		}
		if(node instanceof ListNode) {
			ListNode ln = ((ListNode)node);
			for(int i =0;i<ln.items.size();i++)
			{
				if(Contains(cls, ln.items.get(i)))
					return true;
			}
			return false;
		}
		if(node instanceof MemberExpressionNode) {
			return Contains(cls, ((MemberExpressionNode)node).base) || Contains(cls, ((MemberExpressionNode)node).selector);
		}
		return false;
	}
	public static SimpleAssignment SimplifyAssignment(ExpressionStatementNode expression) {
		SimpleAssignment assignment = new SimpleAssignment();
		assignment.simplify(expression);
		return assignment;
	}
	public static Node GetAssignmentRHS(ExpressionStatementNode node) {
		AstAnalyzer anal = new AstAnalyzer();
		ArrayList<Node> found = anal.FindRecursive((Node) node, new SearchEvaluator.Locator() {
			
			@Override
			public Node apply(SearchEvaluator _this, Node N) throws DoNotRecurseException {
				if(N instanceof SetExpressionNode)
					return N;
				return null;
			}
		});
		for(Node n : found) {
			SetExpressionNode SEN = (SetExpressionNode)n;
			return SEN.args.items.get(0);
		}
		return null;
	}


	public static String Node2String(Node node) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		CoffeePrinter cp = new CoffeePrinter(pw);
		ContextStatics statics = new ContextStatics();
		Context cx = new Context(statics);
		node.evaluate(cx, cp);
		cp.out.beginLine();
		//pw.flush();
		return sw.toString().trim();
	}

	public static Node stripMethodFromMXN(MemberExpressionNode root_mxn, Node defaultNode) {
		if(root_mxn.base!=null) {
			return root_mxn.base;
		}else{
			if(defaultNode != null) {
				return defaultNode;
			}
			return new LiteralNullNode();
		}
	}

	public static boolean ensureImportExists(ProgramNode program, String importID) {
		ArrayList<String> allImports = new ArrayList<String>();
		int indexOfLastImport = 0;
		PackageDefinitionNode pkg=null;
		for(int i =0;i<program.statements.items.size();i++) {
			Node n = program.statements.items.get(i);
			if(n instanceof ImportDirectiveNode) {
				PackageNameNode name = ((ImportDirectiveNode)n).name;
				String iid="";
				for (int j = 0; j < name.id.list.size(); j++) {
					if (j > 0) {
						iid+=".";
					}
					iid+=(name.id.list.get(j).name);
				}
				allImports.add(iid);
				//System.out.println(iid);
				indexOfLastImport = i+1;
			}
		}
		if(program.pkgdefs!=null && !program.pkgdefs.isEmpty()) {
			pkg = program.pkgdefs.get(0);
		}
		if(!allImports.contains(importID)) {
			PackageNameNode pnn = new PackageNameNode(new PackageIdentifiersNode(new IdentifierNode(importID, 0), 0, true), 0);
			ImportDirectiveNode idn = new ImportDirectiveNode(null, null, pnn, null, null);
			program.statements.items.add(indexOfLastImport, idn);
			return true;
		}
		return false;
	}
}
