/*
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package me.venturega.asanalyze.evaluators;

import java.util.ArrayList;
import java.util.function.Function;

import macromedia.asc.parser.*;
import macromedia.asc.semantics.Value;
import macromedia.asc.util.Context;
import flash.swf.tools.as3.EvaluatorAdapter;

/**
 * A partial pretty printer for generating AS3 from ASC AST's. It's used by data
 * binding when generating watchers for Array elements and function return
 * values. We use a PrettyPrinter to recreate the AS3 for the watcher args.
 */
public class SearchEvaluator extends EvaluatorAdapter implements Tokens {
	public ArrayList<Node> found;
	private Locator locator;
	
	@SuppressWarnings("serial")
	public class DoNotRecurseException extends Exception {
		
	}
	
	public interface Locator {
		Node apply(SearchEvaluator _this, Node n) throws DoNotRecurseException;
	}

	public SearchEvaluator(Locator locator) {
		this.locator = locator;
		this.found = new ArrayList<Node>();
	}

	private boolean Check(Node n) {
		//System.out.println(n.getClass().getSimpleName());
		try {
			Node fn = locator.apply(this, n);
			if (fn != null) {
				found.add(fn);
			}
			return true;
		} catch(DoNotRecurseException dnr) { // Ignore warning.
			return false;
		}
	}

	public void stopRecursion() throws DoNotRecurseException {
		throw new DoNotRecurseException();
	}

	@Override
	public Value evaluate(Context cx, ApplyTypeExprNode node) {
		if(!Check(node)) return null;
		if(node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		if(node.typeArgs != null) {
			node.typeArgs.evaluate(cx, this);
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, BreakStatementNode node) {
		if(!Check(node)) return null;
		if(node.id != null)
			node.id.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, CatchClauseNode node) {
		if(!Check(node)) return null;
		if(node.parameter != null)
			node.parameter.evaluate(cx, this);
		if(node.statements != null)
			node.statements.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ClassNameNode node) {
		if(!Check(node)) return null;
		if(node.pkgname != null)
			node.pkgname.evaluate(cx, this);
		if(node.ident!= null)
			node.ident.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, CoerceNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ConditionalExpressionNode node) {
		if(!Check(node)) return null;
		if(node.condition != null)
			node.condition.evaluate(cx, this);
		if(node.thenexpr != null)
			node.thenexpr.evaluate(cx, this);
		if(node.elseexpr != null)
			node.elseexpr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ConfigNamespaceDefinitionNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.metaData != null)
			node.metaData.evaluate(cx, this);
		if(node.name != null)
			node.name.evaluate(cx, this);
		if(node.pkgdef != null)
			node.pkgdef.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ContinueStatementNode node) {
		if(!Check(node)) return null;
		if(node.id != null)
			node.id.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, DeleteExpressionNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, DoStatementNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		if(node.statements != null)
			node.statements.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ExpressionStatementNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, FinallyClauseNode node) {
		if(!Check(node)) return null;
		if(node.default_catch != null)
			node.default_catch.evaluate(cx, this);
		if(node.statements != null)
			node.statements.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, FunctionNameNode node) {
		if(!Check(node)) return null;
		if(node.identifier != null)
			node.identifier.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, FunctionSignatureNode node) {
		if(!Check(node)) return null;
		if(node.inits != null)
			node.inits.evaluate(cx, this);
		if(node.parameter != null)
			node.parameter.evaluate(cx, this);
		if(node.result != null)
			node.result.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ImportDirectiveNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.metaData != null)
			node.metaData.evaluate(cx, this);
		if(node.name != null)
			node.name.evaluate(cx, this);
		if(node.pkgdef != null)
			node.pkgdef.evaluate(cx, this);
		if(node.pkg_node != null)
			node.pkg_node.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ImportNode node) {
		if(!Check(node)) return null;
		if(node.filespec != null)
			node.filespec.evaluate(cx, this);
		if(node.program != null)
			node.program.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, IncludeDirectiveNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.metaData != null)
			node.metaData.evaluate(cx, this);
		if(node.filespec != null)
			node.filespec.evaluate(cx, this);
		if(node.pkgdef != null)
			node.pkgdef.evaluate(cx, this);
		if(node.original_filespec != null)
			node.original_filespec.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, IncrementNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, InheritanceNode node) {
		if(!Check(node)) return null;
		if(node.baseclass != null)
			node.baseclass.evaluate(cx, this);
		if(node.interfaces != null)
			node.interfaces.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, InvokeNode node) {
		if(!Check(node)) return null;
		if(node.args != null)
			node.args.evaluate(cx, this);
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, LabeledStatementNode node) {
		if(!Check(node)) return null;
		if(node.label != null)
			node.label.evaluate(cx, this);
		if(node.statement != null)
			node.statement.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, NamespaceDefinitionNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.metaData != null)
			node.metaData.evaluate(cx, this);
		if(node.name != null)
			node.name.evaluate(cx, this);
		if(node.pkgdef != null)
			node.pkgdef.evaluate(cx, this);
		if(node.value != null)
			node.value.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, PackageDefinitionNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.metaData != null)
			node.metaData.evaluate(cx, this);
		if(node.name != null)
			node.name.evaluate(cx, this);
		//if(node.pkgdef != null)
		//	node.pkgdef.evaluate(cx, this);
		//if(node.statements != null)
		//	node.statements.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, PackageIdentifiersNode node) {
		if(!Check(node)) return null;
		return null;
	}

	@Override
	public Value evaluate(Context cx, PackageNameNode node) {
		if(!Check(node)) return null;
		if(node.id != null)
			node.id.evaluate(cx, this);
		if(node.url!= null)
			node.url.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParameterListNode node) {
		if(!Check(node)) return null;
		if(node.items != null) {
			for(int i = 0, size = node.items.size(); i < size; i++) {
				Node n = node.items.get(i);
				if(n != null) {
					n.evaluate(cx, this);
				}
			}
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParameterNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.identifier != null)
			node.identifier.evaluate(cx, this);
		if(node.init != null)
			node.init.evaluate(cx, this);
		if(node.type != null)
			node.type.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParenExpressionNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ParenListExpressionNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, PragmaExpressionNode node) {
		if(!Check(node)) return null;
		if(node.arg != null)
			node.arg.evaluate(cx, this);
		if(node.identifier != null)
			node.identifier.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, PragmaNode node) {
		if(!Check(node)) return null;
		if(node.list != null)
			node.list.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, QualifiedExpressionNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		if(node.qualifier != null)
			node.qualifier.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, RestExpressionNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, RestParameterNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.identifier != null)
			node.identifier.evaluate(cx, this);
		if(node.init != null)
			node.init.evaluate(cx, this);
		if(node.parameter != null)
			node.parameter.evaluate(cx, this);
		if(node.type != null)
			node.type.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, StatementListNode node) {
		if(!Check(node)) return null;
		if(node.config_attrs != null)
			node.config_attrs.evaluate(cx, this);
		if(node.items != null) {
			for(int i = 0, size = node.items.size(); i < size; i++) {
				Node n = node.items.get(i);
				if(n != null) {
					n.evaluate(cx, this);
				}
			}
		}
		return null;
	}

	@Override
	public Value evaluate(Context cx, SuperExpressionNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, SuperStatementNode node) {
		if(!Check(node)) return null;
		if(node.call != null)
			node.call.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, ThrowStatementNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, TryStatementNode node) {
		if(!Check(node)) return null;
		if(node.catchlist != null)
			node.catchlist.evaluate(cx, this);
		if(node.finallyblock != null)
			node.finallyblock.evaluate(cx, this);
		if(node.tryblock != null)
			node.tryblock.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, TypeExpressionNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, UnaryExpressionNode node) {
		if(!Check(node)) return null;
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, UntypedVariableBindingNode node) {
		if(!Check(node)) return null;
		if(node.identifier != null)
			node.identifier.evaluate(cx, this);
		if(node.initializer != null)
			node.initializer.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, UseDirectiveNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.metaData != null)
			node.metaData.evaluate(cx, this);
		if(node.expr != null)
			node.expr.evaluate(cx, this);
		if(node.pkgdef != null)
			node.pkgdef.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, UseNumericNode node) {
		if(!Check(node)) return null;
		if(node.argument != null)
			node.argument.evaluate(cx, this);
		if(node.identifier != null)
			node.identifier.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, UsePrecisionNode node) {
		if(!Check(node)) return null;
		if(node.argument != null)
			node.argument.evaluate(cx, this);
		if(node.identifier != null)
			node.identifier.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, UseRoundingNode node) {
		if(!Check(node)) return null;
		if(node.argument != null)
			node.argument.evaluate(cx, this);
		if(node.identifier != null)
			node.identifier.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, VariableBindingNode node) {
		if(!Check(node)) return null;
		if(node.attrs != null)
			node.attrs.evaluate(cx, this);
		if(node.initializer != null)
			node.initializer.evaluate(cx, this);
		if(node.variable != null)
			node.variable.evaluate(cx, this);
		return null;
	}

	public Value evaluate(Context cx, AttributeListNode node) {
		if(!Check(node)) return null;;
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			n.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ArgumentListNode node) {
		if(!Check(node)) return null;;
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			n.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, BinaryExpressionNode node) {
		if(!Check(node)) return null;;
		if (node.lhs != null) {
			node.lhs.evaluate(cx, this);
		}

		if (node.rhs != null) {
			node.rhs.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, CallExpressionNode node) {
		if(!Check(node)) return null;;
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		if (node.args != null) {
			node.args.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, SwitchStatementNode node) {
		if(!Check(node)) return null;;
		if (node.expr != null) {
			node.expr.evaluate(cx, this);
		}
		if (node.statements != null) {
			node.statements.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, CaseLabelNode node) {
		if(!Check(node)) return null;;
		if (node.label != null) {
			node.label.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ClassDefinitionNode node) {
		if(!Check(node)) return null;;
		if (node.attrs != null) {
			node.attrs.evaluate(cx, this);
		}

		if ((node.name != null) && (node.name.name != null)) {
			node.name.evaluate(cx, this);
		} else if ((node.cframe != null) && (node.cframe.builder != null)) {
			//
		} else {
			assert false : "No class name found for ClassDefinitionNode";
		}

		if (node.baseclass != null) {
			node.baseclass.evaluate(cx, this);
		}
		if (node.interfaces != null) {
			node.interfaces.evaluate(cx, this);
		}
		if (node.statements != null) {
			node.statements.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ForStatementNode node) {
		if(!Check(node)) return null;;
		if (node.initialize != null) {
			node.initialize.evaluate(cx, this);
		}
		if (node.test != null) {
			node.test.evaluate(cx, this);
		}
		if (node.increment != null) {
			node.increment.evaluate(cx, this);
		}
		if (node.statement != null) {
			node.statement.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, FunctionCommonNode node) {
		if(!Check(node)) return null;;
		if (node.def != null) {
			node.def.evaluate(cx, this);
		}
		if (node.body != null) {
			node.body.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, FunctionDefinitionNode node) {
		if(!Check(node)) return null;;
		if (node.attrs != null) {
			node.attrs.evaluate(cx, this);
		}

		if (node.init != null) {
			node.init.evaluate(cx, this);
		}
		if (node.name!= null) {
			node.name.evaluate(cx, this);
		}
		if (node.pkgdef != null) {
			node.pkgdef.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, GetExpressionNode node) {
		if(!Check(node)) return null;;
		if (node.expr!= null) {
			node.expr.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, IdentifierNode node) {
		if(!Check(node)) return null;;
		return null;
	}

	public Value evaluate(Context cx, IfStatementNode node) {
		if(!Check(node)) return null;
		if (node.condition != null) {
			node.condition.evaluate(cx, this);
		}
		if (node.thenactions != null) {
			node.thenactions.evaluate(cx, this);
		}
		if (node.elseactions!= null) {
			node.elseactions.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, InterfaceDefinitionNode node) {
		if(!Check(node)) return null;
		if (node.attrs!= null) {
			node.attrs.evaluate(cx, this);
		}
		if (node.baseclass != null) {
			node.baseclass.evaluate(cx, this);
		}
		if (node.init != null) {
			node.init.evaluate(cx, this);
		}
		if (node.metaData!= null) {
			node.metaData.evaluate(cx, this);
		}
		if (node.name != null) {
			node.name.evaluate(cx, this);
		}
		if (node.statements!= null) {
			node.statements.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ListNode node) {
		if(!Check(node)) return null;
		for(int i = 0, size = node.items.size(); i < size; i++) {
			if (node.items.get(i) != null) {
				node.items.get(i).evaluate(cx, this);
			}
		}
		return null;
	}

	public Value evaluate(Context cx, LiteralArrayNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralVectorNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralBooleanNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralFieldNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralNumberNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralObjectNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralStringNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralNullNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralRegExpNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralXMLNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, MemberExpressionNode node) {
		if(!Check(node)) return null;
		if (node.base != null) {
			node.base.evaluate(cx, this);
		}
		if (node.selector != null) {
			node.selector.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ProgramNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, QualifiedIdentifierNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ReturnStatementNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ThisExpressionNode node) {
		if(!Check(node)) return null;
		return null;
	}

	public Value evaluate(Context cx, TypedIdentifierNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, VariableDefinitionNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, WhileStatementNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, WithStatementNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, SetExpressionNode node) {
		if(!Check(node)) return null;
		return super.evaluate(cx, node);
	}
}
