/*
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package me.venturega.asanalyze.evaluators;

import java.util.HashMap;

import flash.swf.tools.as3.EvaluatorAdapter;
import macromedia.asc.parser.*;
import macromedia.asc.semantics.Value;
import macromedia.asc.util.Context;
import macromedia.asc.util.ObjectList;

/**
 * Used to figure out parent/child relationships since Flex doesn't track them
 * for some fucking reason.
 */
public class ParentizerEvaluator extends EvaluatorAdapter implements Tokens {
	public HashMap<Node, Node> childParents = new HashMap<Node, Node>();

	public ParentizerEvaluator() {
	}

	public Node getParentOf(Node n) {
		return childParents.getOrDefault(n, null);
	}

	private void addChild(Node parent, Node child) {
		childParents.put(child, parent);
	}

	public Value evaluate(Context cx, AttributeListNode node) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			addChild(node, n);
			n.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ArgumentListNode node) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = node.items.get(i);
			addChild(node, n);
			n.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, BinaryExpressionNode node) {
		if (node.lhs != null) {
			addChild(node, node.lhs);
			node.lhs.evaluate(cx, this);
		}

		if (node.rhs != null) {
			addChild(node, node.rhs);
			node.rhs.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, CallExpressionNode node) {
		if (node.expr != null) {
			addChild(node, node.expr);
			node.expr.evaluate(cx, this);
		}
		if (node.args != null) {
			addChild(node, node.args);
			node.args.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, SwitchStatementNode node) {
		if (node.expr != null) {
			addChild(node, node.expr);
			node.expr.evaluate(cx, this);
		}
		if (node.statements != null) {
			addChild(node, node.statements);
			node.statements.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, CaseLabelNode node) {
		if (node.label != null) {
			addChild(node, node.label);
			node.label.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ClassDefinitionNode node) {
		if (node.attrs != null) {
			addChild(node, node.attrs);
			node.attrs.evaluate(cx, this);
		}

		if ((node.name != null) && (node.name.name != null)) {
			addChild(node, node.name);
			node.name.evaluate(cx, this);
		} else if ((node.cframe != null) && (node.cframe.builder != null)) {
			//
		} else {
			assert false : "No class name found for ClassDefinitionNode";
		}

		if (node.baseclass != null) {
			addChild(node, node.baseclass);
			node.baseclass.evaluate(cx, this);
		}
		if (node.interfaces != null) {
			addChild(node, node.interfaces);
			node.interfaces.evaluate(cx, this);
		}
		if (node.statements != null) {
			addChild(node, node.statements);
			node.statements.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ForStatementNode node) {
		if (node.initialize != null) {
			addChild(node, node.initialize);
			node.initialize.evaluate(cx, this);
		}
		if (node.test != null) {
			addChild(node, node.test);
			node.test.evaluate(cx, this);
		}
		if (node.increment != null) {
			addChild(node, node.increment);
			node.increment.evaluate(cx, this);
		}
		if (node.statement != null) {
			addChild(node, node.statement);
			node.statement.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, FunctionCommonNode node) {
		if(node.body != null) addChild(node, node.body);
		// loop if(node.def != null) addChild(node, node.def);
		if(node.default_dxns != null) addChild(node, node.default_dxns);
		if(node.identifier != null) addChild(node, node.identifier);
		if(node.signature != null) addChild(node, node.signature);
		if(node.use_stmts != null) addChild(node, node.use_stmts);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, FunctionDefinitionNode node) {
		if (node.attrs != null)
			addChild(node, node.attrs);
		if (node.fexpr != null)
			addChild(node, node.fexpr);
		if (node.init != null)
			addChild(node, node.init);
		if (node.metaData != null)
			addChild(node, node.metaData);
		if (node.name != null)
			addChild(node, node.name);
		if (node.pkgdef != null)
			addChild(node, node.pkgdef);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, GetExpressionNode node) {
		if(node.expr != null) addChild(node, node.expr);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, IdentifierNode node) {
		return null;
	}

	public Value evaluate(Context cx, IfStatementNode node) {
		if(node.condition != null) addChild(node, node.condition);
		if(node.elseactions != null) addChild(node, node.elseactions);
		if(node.thenactions != null) addChild(node, node.thenactions);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, InterfaceDefinitionNode node) {
		if(node.baseclass != null) addChild(node, node.baseclass);
		if(node.attrs != null) addChild(node, node.attrs);
		if(node.init != null) addChild(node, node.init);
		if(node.interfaces != null) addChild(node, node.interfaces);
		if(node.metaData != null) addChild(node, node.metaData);
		if(node.name != null) addChild(node, node.name);
		if(node.pkgdef != null) addChild(node, node.pkgdef);
		if(node.statements != null) addChild(node, node.statements);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ListNode node) {
		for(int i =0,size=node.items.size();i<size;i++)
		{
			Node n = node.items.get(i);
			if(n!=null)
				addChild(node, n);
		}
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralArrayNode node) {
		if(node.elementlist != null) addChild(node, node.elementlist);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralVectorNode node) {
		if(node.elementlist != null) addChild(node, node.elementlist);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralBooleanNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralFieldNode node) {
		if(node.name != null) addChild(node, node.name);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralNumberNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralObjectNode node) {
		if(node.fieldlist != null) addChild(node, node.fieldlist);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralStringNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralNullNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralRegExpNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralXMLNode node) {
		if(node.list != null) addChild(node, node.list);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, MemberExpressionNode node) {
		if(node.base != null) addChild(node, node.base);
		if(node.selector != null) addChild(node, node.selector);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, QualifiedIdentifierNode node) {
		if(node.qualifier != null) addChild(node, node.qualifier);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ReturnStatementNode node) {
		if(node.expr != null) addChild(node, node.expr);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ThisExpressionNode node) {
		return null;
	}

	public Value evaluate(Context cx, TypedIdentifierNode node) {
		if(node.type != null) addChild(node, node.type);
		if(node.identifier != null) addChild(node, node.identifier);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, VariableDefinitionNode node) {
		if(node.attrs != null) addChild(node, node.attrs);
		if(node.list != null) addChild(node, node.list);
		if(node.metaData != null) addChild(node, node.metaData);
		if(node.pkgdef != null) addChild(node, node.pkgdef);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, WhileStatementNode node) {
		if(node.expr != null) addChild(node, node.expr);
		if(node.statement != null) addChild(node, node.statement);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, WithStatementNode node) {
		if(node.expr != null) addChild(node, node.expr);
		if(node.statement != null) addChild(node, node.statement);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, SetExpressionNode node) {
		if(node.args != null) addChild(node, node.args);
		if(node.expr != null) addChild(node, node.expr);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, UnaryExpressionNode node) {
		if(node.expr != null) addChild(node, node.expr);
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ConditionalExpressionNode node) {
		if(node.condition != null) addChild(node, node.condition);
		if(node.thenexpr != null) addChild(node, node.thenexpr);
		if(node.elseexpr != null) addChild(node, node.elseexpr);
		return super.evaluate(cx, node);
	}
	
	public Value evaluate(Context cx, ProgramNode node) {
		if(node.statements != null)  addChild(node, node.statements);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ApplyTypeExprNode node) {
		if(node.expr != null) addChild(node, node.expr);
		if(node.typeArgs != null) addChild(node, node.typeArgs);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, BinaryClassDefNode node) {
		int i;
		if(node.attrs != null) addChild(node, node.attrs);
		if(node.baseclass != null) addChild(node, node.baseclass);
		if(node.clsdefs != null) {
			for(i=0;i<node.clsdefs.size();i++) {
				if(node.clsdefs.get(i)!=null)
				addChild(node,node.clsdefs.get(i));
			}
		}
		if(node.fexprs != null) {
			for(i=0;i<node.fexprs.size();i++) {
				if(node.fexprs.get(i)!=null) {
					addChild(node, node.fexprs.get(i));
				}
			}
		}
		if(node.init != null) addChild(node, node.init);
		if(node.instanceinits != null) {
			for(i=0;i<node.instanceinits.size();i++) {
				if(node.instanceinits.get(i)!=null) {
					addChild(node, node.instanceinits.get(i));
				}
			}
		}
		if(node.interfaces != null) addChild(node, node.interfaces);
		if(node.metaData != null) addChild(node, node.metaData);
		if(node.name != null) addChild(node, node.name);
		if(node.pkgdef != null) addChild(node, node.pkgdef);
		if(node.statements != null) addChild(node, node.statements);
		if(node.staticfexprs != null) {
			for(i=0;i<node.staticfexprs.size();i++) {
				if(node.staticfexprs.get(i)!=null) {
					addChild(node, node.staticfexprs.get(i));
				}
			}
		}
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, BinaryFunctionDefinitionNode node) {
		if(node.attrs != null) addChild(node, node.attrs);
		if(node.fexpr != null) addChild(node, node.fexpr);
		if(node.init != null) addChild(node, node.init);
		if(node.metaData != null) addChild(node, node.metaData);
		if(node.name != null) addChild(node, node.name);
		if(node.pkgdef != null) addChild(node, node.pkgdef);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, BinaryInterfaceDefinitionNode node) {
		int i;
		if(node.attrs!= null) addChild(node, node.attrs);
		if(node.baseclass != null) addChild(node, node.baseclass);
		if(node.clsdefs != null) {
			for(i=0;i<node.clsdefs.size();i++) {
				if(node.clsdefs.get(i)!=null) {
					addChild(node, node.clsdefs.get(i));
				}
			}
		}
		if(node.deferred_subclasses != null) {
			for(i=0;i<node.deferred_subclasses.size();i++) {
				if(node.deferred_subclasses.get(i)!=null) {
					addChild(node, node.deferred_subclasses.get(i));
				}
			}
		}
		if(node.fexprs != null) {
			for(i=0;i<node.fexprs.size();i++) {
				if(node.fexprs.get(i)!=null) {
					addChild(node, node.fexprs.get(i));
				}
			}
		}
		if(node.init!=null) addChild(node, node.init);
		
		if(node.instanceinits != null) {
			for(i=0;i<node.instanceinits.size();i++) {
				if(node.instanceinits.get(i)!=null) {
					addChild(node, node.instanceinits.get(i));
				}
			}
		}
		if(node.interfaces!=null) addChild(node, node.interfaces);
		if(node.metaData!=null) addChild(node, node.metaData);
		if(node.name!=null) addChild(node, node.name);
		if(node.pkgdef!=null) addChild(node, node.pkgdef);
		if(node.statements!=null) addChild(node, node.statements);
		
		if(node.staticfexprs != null) {
			for(i=0;i<node.staticfexprs.size();i++) {
				if(node.staticfexprs.get(i)!=null) {
					addChild(node, node.staticfexprs.get(i));
				}
			}
		}
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, BinaryProgramNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, BoxNode node) {
		if(node.expr!=null) addChild(node, node.expr);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, BreakStatementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, CatchClauseNode node) {
		if(node.parameter!=null) addChild(node, node.parameter);
		if(node.statements!=null) addChild(node, node.statements);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ClassNameNode node) {
		if(node.ident!=null) addChild(node, node.ident);
		if(node.pkgname!=null) addChild(node, node.pkgname);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, CoerceNode node) {
		if(node.expr!=null) addChild(node, node.expr);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ConfigNamespaceDefinitionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ContinueStatementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context context, DefaultXMLNamespaceNode defaultXMLNamespaceNode) {
		// TODO Auto-generated method stub
		return super.evaluate(context, defaultXMLNamespaceNode);
	}

	@Override
	public Value evaluate(Context cx, DeleteExpressionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, DocCommentNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, DoStatementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, EmptyElementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, EmptyStatementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ErrorNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ExpressionStatementNode node) {
		if(node.expr!=null) addChild(node, node.expr);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, FinallyClauseNode node) {
		if(node.default_catch!=null) addChild(node, node.default_catch);
		if(node.statements!=null) addChild(node, node.statements);
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, FunctionNameNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, FunctionSignatureNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, HasNextNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context unused_cx, ImportDirectiveNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(unused_cx, node);
	}

	@Override
	public Value evaluate(Context cx, ImportNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, IncludeDirectiveNode node) {
		addChild(node, node.filespec);
		node.filespec.evaluate(cx, this);
		return null;
	}

	@Override
	public Value evaluate(Context cx, IncrementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, InheritanceNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, InvokeNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, LabeledStatementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, LoadRegisterNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context arg0, MetaDataNode arg1) {
		// TODO Auto-generated method stub
		return super.evaluate(arg0, arg1);
	}

	@Override
	public Value evaluate(Context cx, NamespaceDefinitionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, Node node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, PackageDefinitionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context arg0, PackageIdentifiersNode arg1) {
		// TODO Auto-generated method stub
		return super.evaluate(arg0, arg1);
	}

	@Override
	public Value evaluate(Context cx, PackageNameNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context arg0, ParameterListNode arg1) {
		// TODO Auto-generated method stub
		return super.evaluate(arg0, arg1);
	}

	@Override
	public Value evaluate(Context cx, ParameterNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ParenExpressionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ParenListExpressionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, PragmaExpressionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, PragmaNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, QualifiedExpressionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, RegisterNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, RestExpressionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, RestParameterNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, StatementListNode node) {
		if(node.items!=null) {
			for(int i = 0,size=node.items.size();i<size;i++) {
				if(node.items.get(i)!=null) {
					// for...in statements are always preceded by the initializer.  Because reasons.
					// var a:Type;
					// for ( in things) {...}
					if(node.items.get(i) instanceof VariableDefinitionNode && (i+1)<size && node.items.get(i+1) instanceof ForStatementNode) {
						ForStatementNode fsn = (ForStatementNode)node.items.get(i+1);
						if(fsn.initialize == null){
							fsn.initialize=node.items.get(i);
							node.items.set(i, null);
							continue;
						}
					}
				}
			}
			for(int i = 0,size=node.items.size();i<size;i++) {
			    if(node.items.get(i)!=null) {
					addChild(node, node.items.get(i));
				}
			}
		}
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, StoreRegisterNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, SuperExpressionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, SuperStatementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ThrowStatementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, ToObjectNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, TryStatementNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, TypeExpressionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, UntypedVariableBindingNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, UseDirectiveNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, UseNumericNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, UsePrecisionNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, UseRoundingNode node) {
		// TODO Auto-generated method stub
		return super.evaluate(cx, node);
	}

	@Override
	public Value evaluate(Context cx, VariableBindingNode node) {
		if(node.attrs!=null) addChild(node, node.attrs);
		if(node.initializer!=null) addChild(node, node.initializer);
		if(node.variable!=null) addChild(node, node.variable);
		return super.evaluate(cx, node);
	}
}
