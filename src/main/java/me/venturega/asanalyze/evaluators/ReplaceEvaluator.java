/*
*
*  Licensed to the Apache Software Foundation (ASF) under one or more
*  contributor license agreements.  See the NOTICE file distributed with
*  this work for additional information regarding copyright ownership.
*  The ASF licenses this file to You under the Apache License, Version 2.0
*  (the "License"); you may not use this file except in compliance with
*  the License.  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

package me.venturega.asanalyze.evaluators;

import java.util.function.Function;

import macromedia.asc.parser.*;
import macromedia.asc.semantics.Value;
import macromedia.asc.util.Context;
import flash.swf.tools.as3.EvaluatorAdapter;

public class ReplaceEvaluator extends EvaluatorAdapter implements Tokens {
	private Function<Node, Node> replacer;

	public ReplaceEvaluator(Function<Node, Node> replacer) {
		this.replacer = replacer;
	}

	private Node Transform(Node n) {
		return replacer.apply(n);
	}

	public Value evaluate(Context cx, AttributeListNode node) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = Transform(node.items.get(i));
			if(n != null) {
				n.evaluate(cx, this);
			}
			node.items.set(i, n);
		}
		return null;
	}

	public Value evaluate(Context cx, ArgumentListNode node) {
		for (int i = 0, size = node.items.size(); i < size; i++) {
			Node n = Transform(node.items.get(i));
			if(n != null) {
				n.evaluate(cx, this);
			}
			node.items.set(i, n);
		}
		return null;
	}

	public Value evaluate(Context cx, BinaryExpressionNode node) {
		if (node.lhs != null) {
			node.lhs = Transform(node.lhs);
			if(node.lhs!=null)
				node.lhs.evaluate(cx, this);
		}

		if (node.rhs != null) {
			node.rhs = Transform(node.rhs);
			if(node.rhs!=null)
				node.rhs.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, CallExpressionNode node) {
		if (node.expr != null) {
			node.expr = Transform(node.expr);
			if(node.expr!=null)
				node.expr.evaluate(cx, this);
		}
		if (node.args != null) {
			node.args = (ArgumentListNode) Transform(node.args);
			if(node.args!=null)
				node.args.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, SwitchStatementNode node) {
		if (node.expr != null) {
			node.expr = Transform(node.expr);
			if(node.expr!=null)
				node.expr.evaluate(cx, this);
		}
		if (node.statements != null) {
			node.statements = (StatementListNode) Transform(node.statements);
			if(node.statements!=null)
				node.statements.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, CaseLabelNode node) {
		if (node.label != null) {
			node.label = Transform(node.label);
			if(node.label!=null)
				node.label.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ClassDefinitionNode node) {
		if (node.attrs != null) {
			node.attrs = (AttributeListNode) Transform(node.attrs);
			if(node.attrs!=null)
				node.attrs.evaluate(cx, this);
		}

		if ((node.name != null) && (node.name.name != null)) {
			node.name = (IdentifierNode) Transform(node.name);
			if(node.name!=null)
				node.name.evaluate(cx, this);
		} else if ((node.cframe != null) && (node.cframe.builder != null)) {
			//
		} else {
			assert false : "No class name found for ClassDefinitionNode";
		}

		if (node.baseclass != null) {
			node.baseclass = Transform(node.baseclass);
			if(node.baseclass!=null)
				node.baseclass.evaluate(cx, this);
		}
		if (node.interfaces != null) {
			node.interfaces = (ListNode) Transform(node.interfaces);
			if(node.interfaces!=null)
				node.interfaces.evaluate(cx, this);
		}
		if (node.statements != null) {
			node.statements = (StatementListNode) Transform(node.statements);
			if(node.statements!=null)
				node.statements.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ForStatementNode node) {
		if (node.initialize != null) {
			node.initialize = Transform(node.initialize);
			if(node.initialize!=null)
				node.initialize.evaluate(cx, this);
		}
		if (node.test != null) {
			node.test = Transform(node.test);
			if(node.test!=null)
				node.test.evaluate(cx, this);
		}
		if (node.increment != null) {
			node.increment = Transform(node.increment);
			if(node.increment!=null)
				node.increment.evaluate(cx, this);
		}
		if (node.statement != null) {
			node.statement = Transform(node.statement);
			if(node.statement!=null)
				node.statement.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, FunctionCommonNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, FunctionDefinitionNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, GetExpressionNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, IdentifierNode node) {
		return null;
	}

	public Value evaluate(Context cx, IfStatementNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, InterfaceDefinitionNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ListNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralArrayNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralVectorNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralBooleanNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralFieldNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralNumberNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralObjectNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralStringNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralNullNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralRegExpNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, LiteralXMLNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, MemberExpressionNode node) {
		if (node.base != null) {
			node.base = Transform(node.base);
			if(node.base!=null)
				node.base.evaluate(cx, this);
		}
		if (node.selector != null) {
			node.selector = (SelectorNode) Transform(node.selector);
			if(node.selector!=null)
				node.selector.evaluate(cx, this);
		}
		return null;
	}

	public Value evaluate(Context cx, ProgramNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, QualifiedIdentifierNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ReturnStatementNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, ThisExpressionNode node) {
		return null;
	}

	public Value evaluate(Context cx, TypedIdentifierNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, VariableDefinitionNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, WhileStatementNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, WithStatementNode node) {
		return super.evaluate(cx, node);
	}

	public Value evaluate(Context cx, SetExpressionNode node) {
		return super.evaluate(cx, node);
	}
}

