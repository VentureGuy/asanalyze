package me.venturega.asanalyze.codegen;

import java.util.HashMap;
import java.util.function.Consumer;
import java.util.function.Function;

import macromedia.asc.parser.ArgumentListNode;
import macromedia.asc.parser.IdentifierNode;
import macromedia.asc.parser.Node;
import macromedia.asc.parser.NodeFactory;
import macromedia.asc.parser.StatementListNode;
import me.venturega.asanalyze.evaluators.SearchEvaluator;
import me.venturega.asanalyze.evaluators.SearchEvaluator.DoNotRecurseException;
import me.venturega.asanalyze.utils.AstAnalyzer;
import me.venturega.asanalyze.utils.IndentPrinter;

public class CoffeeClass {
	public HashMap<String, CoffeeVariable> attributes = new HashMap<String, CoffeeVariable>();
	public HashMap<String, CoffeeMethod> methods = new HashMap<String, CoffeeMethod>();
	public String extendsType = null;
	public String name;
	public String documentation = null;

	public HashMap<String, String> nameMappings = new HashMap<String, String>();
	// private Context cx;
	// private NodeFactory factory;

	public void printClass(IndentPrinter w) {
		if (this.documentation != null) {
			for (String chunk : documentation.split("[\r\n]+")) {
				w.beginLine().println(String.format("# %s", chunk));
			}
		}
		w.beginLine().print(String.format("class %s", name));
		if (this.extendsType != null) {
			w.print(String.format(" extends %s", extendsType));
		}
		w.println("").indent();
		{
			if (methods.containsKey(name)) {
				CoffeeMethod m = methods.get(name);
				m.name = "constructor";
				methods.put("constructor", m);
			}
			if (!methods.containsKey("constructor")) {
				methods.put("constructor", new CoffeeMethod("constructor"));
			}
			CoffeeMethod ctor = methods.get("constructor");
			NodeFactory factory = ctor.getFactory();
			StatementListNode oldbody = ctor.body;
			StatementListNode newbody = factory.statementList(null, null);
			for (CoffeeVariable cv : attributes.values()) {
				newbody.items.add(factory.expressionStatement(factory.memberExpression(null, factory.setExpression(
						factory.identifier(cv.name), makeNodeFromValue(factory, cv.initializer), false))));
			}
			for (int i = 0, size = oldbody.items.size(); i < size; i++) {
				newbody.items.add(oldbody.items.get(i));
			}
			ctor.body = newbody;
			ctor.WriteIndented(factory.getContext(), w);

			for (CoffeeMethod meth : methods.values()) {
				if (meth.name == "constructor") {
					continue;
				}
				meth.WriteIndented(factory.getContext(), w);
			}
		}
		w.dedent();
	}

	public void Do(final Consumer<Node> action) {
		for (CoffeeMethod m : methods.values()) {
			(new AstAnalyzer()).FindRecursive(m.body, new SearchEvaluator.Locator() {
				
				@Override
				public Node apply(SearchEvaluator _this, Node n) throws DoNotRecurseException {
					action.accept(n);
					return n;
				}
			});
		}
	}

	public void FindAll(final SearchEvaluator.Locator function) {
		for (CoffeeMethod m : methods.values()) {
			(new AstAnalyzer()).FindRecursive(m.body, function);
		}
	}

	public void fixIdentifiers() {
		Do(new Consumer<Node>() {
			public void accept(Node n) {
				switch (n.getClass().getSimpleName()) {
				case "IdentifierNode":
					IdentifierNode ident = (IdentifierNode) n;
					ident.name = fixIdentifierName(ident.name);
					break;
				}
			}
		});
	}

	private String fixIdentifierName(String name) {
		return nameMappings.getOrDefault(name, name);
	}

	private ArgumentListNode makeNodeFromValue(NodeFactory factory, Object initializer) {
		Node n = null;
		if (initializer instanceof Integer || initializer instanceof Double) {
			n = factory.literalNumber(initializer.toString());
		}
		if (initializer instanceof String) {
			n = factory.literalString((String) initializer);
		}
		return factory.argumentList(null, n);
	}
}
