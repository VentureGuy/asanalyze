package me.venturega.asanalyze.codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

import flex2.compiler.as3.AbstractSyntaxTreeUtil;
import macromedia.asc.parser.ArgumentListNode;
import macromedia.asc.parser.CallExpressionNode;
import macromedia.asc.parser.IdentifierNode;
import macromedia.asc.parser.Node;
import macromedia.asc.parser.NodeFactory;
import macromedia.asc.parser.StatementListNode;
import macromedia.asc.util.Context;
import macromedia.asc.util.ContextStatics;
import me.venturega.asanalyze.evaluators.SearchEvaluator;
import me.venturega.asanalyze.evaluators.SearchEvaluator.DoNotRecurseException;
import me.venturega.asanalyze.utils.AstAnalyzer;
import me.venturega.asanalyze.utils.CoffeePrinter;
import me.venturega.asanalyze.utils.IndentPrinter;

public class CoffeeMethod {
	public String name;
	public ArrayList<Argument> arguments = new ArrayList<Argument>();
	public StatementListNode body;
	public String documentation;

	public HashMap<String, String> nameMappings = new HashMap<String, String>();

	private Context cx;
	private NodeFactory factory;

	public class Argument {
		public Argument(String name, boolean isthis, String defaultValue) {
			this.name = name;
			this.isThis = isthis;
			this.defaultValue = defaultValue;
		}

		public Argument(String name, boolean isthis) {
			this(name, isthis, "");
		}

		public Argument(String name) {
			this(name, false, "");
		}

		public String name;
		public String defaultValue = null;
		public boolean isThis = false;

		@Override
		public String toString() {
			String o = name;
			if (isThis)
				o = "@" + o;
			if (defaultValue != "")
				o += "=" + defaultValue;
			return o;
		}
	}

	public CoffeeMethod(String name) {
		this.name = name;
		cx = new Context(new ContextStatics());
		factory = new NodeFactory(cx);
		body = factory.statementList(null, (StatementListNode) null);
	}
	
	public Context getContext() {
		return cx;
	}
	
	public NodeFactory getNodeFactory() {
		return factory;
	}

	public CoffeeMethod addArgument(String name, boolean isthis, String defaultValue) {
		arguments.add(new Argument(name, isthis, defaultValue));
		return this;
	}

	public CoffeeMethod addArgument(String name) {
		addArgument(name, false, "");
		return this;
	}

	public CoffeeMethod addArgument(String name, boolean isthis) {
		addArgument(name, isthis, "");
		return this;
	}

	public void WriteIndented(Context cx, IndentPrinter w) {
		printSignature(w);
		w.indent();
		{
			body.evaluate(cx, new CoffeePrinter(w));
		}
		w.dedent();
	}

	public void printSignature(IndentPrinter w) {
		ArrayList<String> arglist = new ArrayList<String>();
		for (Argument a : arguments) {
			arglist.add(a.toString());
		}
		for (String docline : documentation.split("[\r\n]+")) {
			w.beginLine().println("# " + docline);
		}
		w.beginLine().println(
				String.format("%s: (%s) %s", name, String.join(", ", arglist), name == "constructor" ? "->" : "=>"));
	}

	public NodeFactory getFactory() {
		return factory;
	}

	public void generateSuper() {
		ArgumentListNode args = null;
		if (arguments.size() > 0) {
			args = factory.argumentList(null, null);
			args.items.clear();
			for (Argument arg : arguments) {
				args.items.add((Node) AbstractSyntaxTreeUtil.generateGetterSelector(factory, arg.name, false));
			}
		}
		generateSuper(args);
	}

	public void generateSuper(ArgumentListNode args) {
		if (name == "constructor") {
			body.items.add(factory.superStatement(
					(CallExpressionNode) factory.callExpression(factory.superExpression(null, -1), args)));
		} else {
			// See
			// F:\Projects\flex-sdk\modules\compiler\src\java\flex2\compiler\mxml\ImplementationGenerator.java
			// @ generateInitializeFunction
			body.items.add(factory
					.expressionStatement(factory.list(null, factory.memberExpression(factory.superExpression(null, -1),
							(CallExpressionNode) factory.callExpression(factory.identifier(name), args)))));
		}
	}

	@SuppressWarnings("unused")
	private Node fixIdentifiers(Node node) {
		(new AstAnalyzer()).FindRecursive(node, new SearchEvaluator.Locator() {
			
			@Override
			public Node apply(SearchEvaluator _this, Node n) throws DoNotRecurseException {
				switch (n.getClass().getSimpleName()) {
				case "IdentifierNode":
					IdentifierNode ident = (IdentifierNode) n;
					ident.name = fixIdentifierName(ident.name);
					break;
				}
				return n;
			}
		});
		return node;
	}

	private String fixIdentifierName(String name) {
		return nameMappings.getOrDefault(name, name);
	}

	@SuppressWarnings("unused")
	private ArgumentListNode makeNodeFromValue(NodeFactory factory, Object initializer) {
		Node n = null;
		if (initializer instanceof Integer || initializer instanceof Double) {
			n = factory.literalNumber(initializer.toString());
		}
		if (initializer instanceof String) {
			n = factory.literalString((String) initializer);
		}
		return factory.argumentList(null, n);
	}
}
