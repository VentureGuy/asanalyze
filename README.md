# Apache Flex AST Analysis Toolkit
*Not associated with the Apache Foundation*

The ASTTK is used in several projects that need to parse, analyze, and rewrite
ActionScript 3 files.

## Features

ASTTK defines several useful features, including:

* AS3 -> CoffeeScript 1 writer
* An ActionScript 3 pretty-printer (the one included in Flex is incomplete)
  * WIP
* XPath-like AST query structures
* Indentation management for pretty-printing files

## Dependencies

ASTTK requires Java, Apache Flex, Maven, and Python to be installed.

## Compile instructions

1. Use Apache's Mavenizer tool to install Flex's packages to `C:\Maven-Repo`.
  * If you change this, update `pom.xml`.
1. Run `python BUILD.py`.

The `dist`/ diectory should now contain a few shell scripts, a `.jar`, and a bundle of libraries in `dist/lib/`.